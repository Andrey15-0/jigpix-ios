//
//  Protocols.swift
//  JigsawPuzzleApp
//
//  Created by Ivan Yavorin on 12/5/15.
//  Copyright © 2015 JigSaw. All rights reserved.
//

import Foundation
import UIKit

protocol ImagePickerDelegate : class {
    
    func picker(_ imagePicker: UIViewController, didSelectImage image:UIImage?)
    var fullSizeImage:UIImage? {get set}
}
