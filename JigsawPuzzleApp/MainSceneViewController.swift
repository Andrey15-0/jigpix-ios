//
//  ViewController.swift
//  JigsawPuzzleApp
//
//  Created by Ivan Yavorin on 12/2/15.
//  Copyright © 2015 JigSaw. All rights reserved.
//

import UIKit
/**
 The main app user interface for displaying selected image, scaling, image processing progress and processing result
 */
class MainSceneViewController: ActionsViewController, UIScrollViewDelegate, JigpixAlgoritmDelegate {
    
    @IBOutlet weak var fullSizeBlurredImageView:UIImageView! //used as background
    @IBOutlet weak var scrollView:UIScrollView!
    
    @IBOutlet weak var blurEffectView:UIVisualEffectView!
    @IBOutlet weak var bottomButtonsHolderView:UIView!
    @IBOutlet weak var coordinateGridView:CoordinateGridView!
    
    var scrollViewRatioConstraint:NSLayoutConstraint?
    
    @IBOutlet weak var verticalSpaceToMainImageConstraint:NSLayoutConstraint!
    
    @IBOutlet weak var convertButton:FlexibleTitleButton!
    @IBOutlet weak var convertButtonLeadingConstraint:NSLayoutConstraint!
    @IBOutlet weak var convertButtonTrailingConstraint:NSLayoutConstraint!
    
    @IBOutlet weak var leftImageView:UIImageView!
    @IBOutlet weak var rightImageView:UIImageView!
    
    @IBOutlet weak var backButton:UIButton!
    @IBOutlet weak var photoButton:UIButton!
    @IBOutlet weak var actionButton:UIButton!
    @IBOutlet weak var infoButton:UIButton!
    @IBOutlet weak var convertHolderView: UIView!
    
    @IBOutlet weak var puzzlePieceQuantityIndicator:UIImageView!;
    
    fileprivate lazy var algorithm: JigpixAlgoritm? = nil;
    
    var mainImageView:UIImageView?
    var doubleTapRecognizer:UITapGestureRecognizer?
    internal var fullSizeImage:UIImage?
    internal var puzzleImageIdx:Int = 0;
    var imageToProcess:UIImage? //will hold user picked image
    var targetImage:UIImage = UIImage() {
        didSet{
            if targetImage.size.width > 0
            {
                mainImageView?.image = targetImage
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        // by double tapping on image - in becomes smaller a little bit and centers itself in scrollView
        doubleTapRecognizer = UITapGestureRecognizer (target: self, action: #selector(MainSceneViewController.doubleTapAction(_:)))
        
        doubleTapRecognizer?.numberOfTapsRequired = 2
        doubleTapRecognizer?.numberOfTouchesRequired = 1
        scrollView.addGestureRecognizer(doubleTapRecognizer!)
        
        self.navigationItem.hidesBackButton = true
        
        bottomButtonsHolderView.layer.borderColor = UIColor.white.cgColor
        bottomButtonsHolderView.layer.borderWidth = 1.0
        
        fullSizeBlurredImageView.image = self.fullSizeImage
        convertHolderView.clipsToBounds = true
        
        let puzzleImage = self.getPuzzleImageById(puzzleImageIdx).rotate(90);
        self.backButton.setImage(puzzleImage, for: UIControlState());
        self.backButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.fill;
        self.backButton.contentVerticalAlignment = UIControlContentVerticalAlignment.fill;
        self.backButton.imageEdgeInsets = UIEdgeInsetsMake(4,0,-8,0);
        self.backButton.imageView?.contentMode = .scaleToFill;
        
        self.convertButton.titleState = .convert(title: "Convert");
        
        let medium = UIImage(named: "520_icon.png")!.withRenderingMode(UIImageRenderingMode.alwaysTemplate);
        let small = UIImage(named: "300_icon.png")!.withRenderingMode(UIImageRenderingMode.alwaysTemplate);
        self.puzzlePieceQuantityIndicator.image =  JigpixAlgoritm.puzzlePieces == Pieces.medium520 ? medium : small;
        self.puzzlePieceQuantityIndicator.tintColor = UIColor.white;
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning();
        
        // Dispose of any resources that can be recreated.
        self.algorithm?.stopWorking = true;
        switch self.convertButton.titleState
        {
        case .converting:
            self.showMemoryOutOfMemoryMessage();
        default:
            break;
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        
        if(self.needRefresh)
        {
            //hide left and right small image views
            convertHolderView.isHidden = true;
            
            let viewHeight = self.view.bounds.size.height;
            if viewHeight < 500
            {
                verticalSpaceToMainImageConstraint.constant = 8.0;
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
        
        if(self.needRefresh)
        {
            self.convertHolderView.isHidden = false;
            
            addImageHolder(self.fullSizeImage);
            
            self.piecesQuantityButtonTapped();
        }
        else
        {
            self.needRefresh = true;
        }
    }
    
    override var prefersStatusBarHidden : Bool {
        return true;
    }
    
    //MARK: - Button actions
    @IBAction func backButtonTapped(_ sender: UIButton)
    {
        self.algorithm?.stopWorking = true;
        self.dismiss(animated: true, completion: nil);
    }
    
    @IBAction func infoButtonTapped(_ sender: UIButton)
    {
        showInfoDialog(sender);
    }
    
    @IBAction func actionButtonTapped(_ sender: UIButton)
    {
        let outputImage = createImageForSaving();
        showActionsDialog(sender, image: outputImage);
    }
    
    func createImageForSaving() -> UIImage
    {
        switch convertButton.titleState
        {
        case .done:
            let logo = UIImage(named: "Logo_Final")!;
            let puzzle = self.getPuzzleImageById(puzzleImageIdx).roundCorners();
            let selfie = self.leftImageView.image!.roundCorners();
            let result = self.rightImageView.image!.roundCorners();
            
            UIGraphicsBeginImageContextWithOptions(self.coordinateGridView.bounds.size, false, 0);
            self.coordinateGridView.drawHierarchy(in: self.coordinateGridView.bounds, afterScreenUpdates: true);
            var image:UIImage = UIGraphicsGetImageFromCurrentImageContext()!;
            UIGraphicsEndImageContext();
            image = image.resizeImage2(CGSize(width: 1120.0, height: 1408.0));
            
            let heightOffset = image.size.height / 5.0;
            let sizeResult = CGSize(width: image.size.width, height: image.size.height + heightOffset);
            let opaque = false;
            let scale: CGFloat = 0;
            UIGraphicsBeginImageContextWithOptions(sizeResult, opaque, scale);
            let context = UIGraphicsGetCurrentContext();
            
            let borderRect = CGRect(x: 0, y: 0, width: sizeResult.width, height: sizeResult.height);
            context?.setFillColor(UIColor.gray.cgColor);
            context?.addRect(borderRect);
            context?.drawPath(using: CGPathDrawingMode.fill);
            
            let border: CGFloat = 40.0;
            let mainRect = CGRect(x: border, y: border, width: sizeResult.width - 2.0 * border, height: sizeResult.height - 2.0 * border);
            context?.setFillColor(UIColor.white.cgColor);
            context?.addRect(mainRect);
            context?.drawPath(using: CGPathDrawingMode.fill);
            
            let yOffset: CGFloat = 5.0;
            //Draw logo
            let logoRect = CGRect(x: border, y: yOffset + border + 10, width: image.size.width / 2.5 - border, height: heightOffset - yOffset - 30);
            logo.draw(in: logoRect);
            
            let smallImageWidth: CGFloat = (image.size.width - image.size.width / 2.5) / 3.0 - 40;
            
            //Draw puzzle
            let puzzleRect = CGRect(x: image.size.width / 2.5, y: yOffset + border, width: smallImageWidth, height: heightOffset - yOffset);
            puzzle.draw(in: puzzleRect);
            
            context?.setLineWidth(3.0);
            context?.setStrokeColor(UIColor.red.cgColor);
            
            //Draw plus
            context?.move(to: CGPoint(x: puzzleRect.origin.x + smallImageWidth + 2, y: heightOffset / 2.0 + border));
            context?.addLine(to: CGPoint(x: puzzleRect.origin.x + smallImageWidth + 2 + 20, y: heightOffset / 2.0 + border));
            context?.strokePath();
            context?.move(to: CGPoint(x: puzzleRect.origin.x + smallImageWidth + 2 + 10, y: heightOffset / 2.0 + border - 10));
            context?.addLine(to: CGPoint(x: puzzleRect.origin.x + smallImageWidth + 2 + 10, y: heightOffset / 2.0 + border + 10));
            context?.strokePath();
            
            //Draw selfie
            let selfieRect = CGRect(x: image.size.width / 2.5 + smallImageWidth + 2 + 20 + 2, y: puzzleRect.origin.y, width: smallImageWidth, height: heightOffset - yOffset);
            selfie.draw(in: selfieRect);
            
            //Draw equal
            context?.move(to: CGPoint(x: selfieRect.origin.x + smallImageWidth + 2, y: heightOffset / 2.0 - 8 + border));
            context?.addLine(to: CGPoint(x: selfieRect.origin.x + smallImageWidth + 2 + 20, y: heightOffset / 2.0 - 8 + border));
            context?.strokePath();
            context?.move(to: CGPoint(x: selfieRect.origin.x + smallImageWidth + 2, y: heightOffset / 2.0 + 4 + border));
            context?.addLine(to: CGPoint(x: selfieRect.origin.x + smallImageWidth + 2 + 20, y: heightOffset / 2.0 + 4 + border));
            context?.strokePath();
            
            //Draw result
            let resultRect = CGRect(x: selfieRect.origin.x + smallImageWidth + 2 + 20 + 2, y: puzzleRect.origin.y, width: smallImageWidth, height: heightOffset - yOffset);
            result.draw(in: resultRect);
            
            //Draw image
            let imageRect = CGRect(x: border, y: heightOffset + border, width: image.size.width - 2.0 * border, height: image.size.height - 2.0 * border);
            image.draw(in: imageRect);
            
            let outputImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            return outputImage!;
        default:
            UIGraphicsBeginImageContextWithOptions(self.coordinateGridView.bounds.size, false, 0);
            self.coordinateGridView.drawHierarchy(in: self.coordinateGridView.bounds, afterScreenUpdates: true);
            var image:UIImage = UIGraphicsGetImageFromCurrentImageContext()!;
            UIGraphicsEndImageContext();
            image = image.resizeImage2(CGSize(width: 1120.0, height: 1408.0));
            
            return image;
        }
    }
    
    @IBAction func cameraButtonTapped(_ sender: UIButton)
    {
        showCameraDialog(sender);
    }
    
    fileprivate func convertButtonStateWasSet()
    {
        var toAnimateArray = [(constraint:NSLayoutConstraint, constant:CGFloat)]()
        toAnimateArray.append((self.convertButtonLeadingConstraint, 0.0))
        toAnimateArray.append((self.convertButtonTrailingConstraint, 0.0))
        self.animateConstrains(toAnimateArray)
        self.addImageHolder(self.fullSizeImage)
    }
    
    fileprivate func animateAfterButtonTitleWasChanged()
    {
        let currentSmallImagesWidth = self.leftImageView.bounds.size.width
        var toAnimateArray = [(constraint:NSLayoutConstraint, constant:CGFloat)]()
        
        switch convertButton.titleState
        {
        case .convert:
            self.convertButtonStateWasSet();
            self.coordinateGridView.ShowGrid(false, columns: JigpixAlgoritm.COLS, rows: JigpixAlgoritm.ROWS);
            self.algorithm = nil;
        case .converting:
            toAnimateArray.append((self.convertButtonLeadingConstraint, 8.0 + currentSmallImagesWidth))
            toAnimateArray.append((self.convertButtonTrailingConstraint, 0.0))
            if let imageToProcess = self.getCurrentImageToProcess()
            {
                self.leftImageView.image = imageToProcess
                self.addImageHolder(imageToProcess)
                self.startProcess(imageToProcess)
            }
            
            self.animateConstrains(toAnimateArray)
            
        case .getCode:
            toAnimateArray.append((self.convertButtonLeadingConstraint, 8.0 + currentSmallImagesWidth))
            toAnimateArray.append((self.convertButtonTrailingConstraint, 0.0))
            
            self.animateConstrains(toAnimateArray)
            
        case .done:
            toAnimateArray.append((self.convertButtonTrailingConstraint, 8.0 + currentSmallImagesWidth))
            
            self.scrollView.zoomScale = 1.0;
            
            if let imagePixelized = self.getCurrentImageToProcess()
            {
                self.rightImageView.image = imagePixelized;
            }
            
            self.animateConstrains(toAnimateArray);
            self.rotateMainImageView();
            
        }//end switch
        
    }
    
    /// This method is tied up to 2 UITapGestureRecognizer-s in the storyboard
    @IBAction func bottomImageTapped(_ recognizer:UITapGestureRecognizer)
    {
        if !convertButton.isEnabled //disable interaction when currently procecssing the image
        {
            return
        }
        
        switch recognizer.view!
        {
        case self.leftImageView:
            convertButton.switchToDefaultState();
            self.convertButtonStateWasSet();
            self.coordinateGridView.ShowGrid(false, columns: JigpixAlgoritm.COLS, rows: JigpixAlgoritm.ROWS);
        case self.rightImageView:
            switch convertButton.titleState{
            case .done(_):
                self.scrollView.zoomScale = 1.0;
                self.convertButton.setPreviousTitle();
                animateAfterButtonTitleWasChanged();
                rotateMainImageViewWith(rightImageView.image!);
            default:
                break
            }
        default:
            break
        }
    }
    
    @IBAction func convertButtonAction(_ sender:FlexibleTitleButton)
    {
        sender.setNextTitle()
        
        animateAfterButtonTitleWasChanged()
    }
    
    fileprivate func animateConstrains(_ constraintAndValueTuples:[(constraint:NSLayoutConstraint, constant:CGFloat)], completion:(()->())? = nil)
    {
        for (constraint, constantValue) in constraintAndValueTuples{
            constraint.constant = constantValue
        }
        
        if let completionHandler = completion
        {
            UIView.animate(withDuration: 0.25, delay: 0.0, options: UIViewAnimationOptions.curveLinear, animations: { () -> Void in
                self.view.layoutIfNeeded()
                }, completion: { _ in
                    completionHandler()
            })
        }
        else
        {
            UIView.animate(withDuration: 0.25, animations: { _ in
                self.view.layoutIfNeeded()
            }) 
        }
    }
    
    fileprivate func rotateMainImageView()
    {
        let bgQueue = DispatchQueue(label: "CodeImageQueue", attributes: [])
        bgQueue.async{[weak self] in
            guard let image = self?.algorithm?.GetCode() else
            {
                return
            }
            DispatchQueue.main.async{[weak self] in
                self?.rotateMainImageViewWith(image)
            }
        }
    }
    
    fileprivate func rotateMainImageViewWith(_ image:UIImage)
    {
        let layer = mainImageView!.layer;
        var rotationAndPerspectiveTransformStart = CATransform3DIdentity;
        rotationAndPerspectiveTransformStart.m34 = 1.0 / 1000;
        rotationAndPerspectiveTransformStart = CATransform3DRotate(rotationAndPerspectiveTransformStart, CGFloat(Double.pi / 2.0), 0.0, -1.0, 0.0);
        UIView.animate(withDuration: 0.5, animations: {
            layer.transform = rotationAndPerspectiveTransformStart;
            }, completion: {[weak self]
                (value: Bool) in
                self?.targetImage = image;
                layer.transform = CATransform3DIdentity;
                var rotationAndPerspectiveTransformEnd = CATransform3DIdentity;
                rotationAndPerspectiveTransformEnd.m34 = 1.0 / 1000;
                rotationAndPerspectiveTransformEnd = CATransform3DRotate(rotationAndPerspectiveTransformEnd, CGFloat(Double.pi / 2.0), 0.0, 1.0, 0.0);
                layer.transform = rotationAndPerspectiveTransformEnd;
                UIView.animate(withDuration: 0.5, animations: {
                    layer.transform = CATransform3DIdentity;
                });
            });
    }
    
    func piecesQuantityButtonTapped()
    {
        if let overridingRatioConstraint = scrollViewRatioConstraint
        {
            self.scrollView.removeConstraint(overridingRatioConstraint)
            scrollViewRatioConstraint = nil
        }
        
        mainImageView?.removeFromSuperview()
        
        var newRatio:NSLayoutConstraint?;
        switch JigpixAlgoritm.puzzlePieces
        {
        case .small300:
            newRatio = NSLayoutConstraint(item: scrollView, attribute: .width, relatedBy: .equal, toItem: scrollView, attribute: .height, multiplier: 3.0/4.0, constant: 0.0);
        default:
            newRatio = NSLayoutConstraint(item: scrollView, attribute: .width, relatedBy: .equal, toItem: scrollView, attribute: .height, multiplier: 10.0/13.0, constant: 0.0);
            break
        }
        
        guard let newConstraint = newRatio else
        {
            return;
        }
        
        scrollViewRatioConstraint = newConstraint
        
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseIn, animations: { [weak self] () -> Void in
            if let weakSelf = self
            {
                weakSelf.scrollView.addConstraint(newConstraint)
                weakSelf.view.layoutIfNeeded()
            }
            
        }) {[weak self] (_) -> Void in
            if let weakSelf = self
            {
                weakSelf.addImageHolder(weakSelf.fullSizeImage)
                weakSelf.leftImageView.image = nil
                weakSelf.rightImageView.image = nil
            }
            
        }
    }
    
    fileprivate func addImageHolder(_ image:UIImage?)
    {
        guard let fullImage = image else
        {
            print(" - Error: No image found.")
            return
        }
        
        for aSubview in scrollView.subviews
        {
            aSubview.removeFromSuperview()
        }
        
        scrollView.contentSize = scrollView.bounds.size
        
        //create imageView
        mainImageView = UIImageView(image: fullImage)
        
        guard let imageView = self.mainImageView else
        {
            print(" - Error:  Did not create \"mainImageView\". ")
            return
        }
        
        //position imageView to zoom and pan in scrollView
        imageView.sizeToFit()
        var fullFrame = imageView.frame;
        var maxScale: CGFloat = 3.0;
        
        if fullFrame.size.width > scrollView.bounds.size.width || fullFrame.size.height > scrollView.bounds.size.height
        {
            // decrease image frame to fit scrollView dimensions
            
            let horizontalRatio = scrollView.bounds.size.width / fullFrame.size.width
            let verticalRatio = scrollView.bounds.size.height / fullFrame.size.height
            
            let scaleFactor = (horizontalRatio > verticalRatio) ? horizontalRatio : verticalRatio
            
            fullFrame.size.width *= scaleFactor
            fullFrame.size.height *= scaleFactor
            
            //setup scrollview insets at the top and bottom, or at the left and right
            var value = CGFloat(0.0)
            scrollView.contentSize = fullFrame.size
            if fullFrame.height == scrollView.bounds.size.height // setup insets to left and right
            {
                let difference = fullFrame.size.width - scrollView.bounds.size.width
                value = difference / 2.0
                scrollView.contentOffset.x = value
                //scrollView.contentInset = UIEdgeInsetsMake(0, value, 0.0, value)
            }
            else if fullFrame.width == scrollView.bounds.size.width //setup insets to top and bottom
            {
                let difference = fullFrame.size.height - scrollView.bounds.size.height
                value = difference / 2.0
                scrollView.contentOffset.y = value
                //scrollView.contentInset = UIEdgeInsetsMake(value, 0.0, value, 0.0)
            }
            
            imageView.frame = fullFrame
            
            maxScale = min(4.0,max(3.0, floor(1 / scaleFactor)))
        }
        else
        {
            imageView.frame = scrollView.bounds
        }
        
        self.scrollView.maximumZoomScale = maxScale;
        self.scrollView.addSubview(imageView);
        
        let timeout:DispatchTime = DispatchTime.now() + Double(Int64(Double(NSEC_PER_SEC) * 1.0)) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: timeout, execute: {[weak self] () -> Void in
            self?.scrollView.flashScrollIndicators()
            })
    }
    
    fileprivate func addGridDrawerViewTo(_ view:UIView?)
    {
        guard let subView  = view else { return }
        let currentSize = ((JigpixAlgoritm.puzzlePieces == .small300) ? CGSize(width: 15, height: 20) : CGSize(width: 20, height: 26))
        let gridBlackAndWhiteView = GridDrawerView(superView: subView, gridCells:currentSize)
        subView.addSubview(gridBlackAndWhiteView)
    }
    
    fileprivate func centerImageInScrollView()
    {
        guard let imageView = mainImageView else
        {
            return
        }
        
        let boundsSize = scrollView.bounds.size;
        var contentsFrame = imageView.frame;
        
        if (contentsFrame.size.width < boundsSize.width)
        {
            contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0;
        }
        else
        {
            contentsFrame.origin.x = 0.0;
        }
        
        
        if (contentsFrame.size.height < boundsSize.height)
        {
            contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0;
        }
        else
        {
            contentsFrame.origin.y = 0.0;
        }
        
        imageView.frame = contentsFrame;
    }
    
    //MARK: UIScrollViewDelegate
    func viewForZooming(in scrollView: UIScrollView) -> UIView?
    {
        return mainImageView
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView)
    {
        self.coordinateGridView.onScrollChanged(scrollView.zoomScale,
                                                leftContentOffset: scrollView.contentOffset.x,
                                                topContentOffset: scrollView.contentOffset.y)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.coordinateGridView.onScrollChanged(scrollView.zoomScale,
                                                leftContentOffset: scrollView.contentOffset.x,
                                                topContentOffset: scrollView.contentOffset.y)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate
        {
            self.coordinateGridView.onScrollChanged(scrollView.zoomScale,
                                                    leftContentOffset: scrollView.contentOffset.x,
                                                    topContentOffset: scrollView.contentOffset.y)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.coordinateGridView.onScrollChanged(scrollView.zoomScale,
                                                leftContentOffset: scrollView.contentOffset.x,
                                                topContentOffset: scrollView.contentOffset.y)
    }
    
    //MARK: UITapRecogniger action
    func doubleTapAction(_ recognizer:UITapGestureRecognizer)
    {
        // Zoom out slightly, capping at the minimum zoom scale specified by the scroll view
        var newZoomScale:CGFloat = scrollView.zoomScale / 1.5
        newZoomScale = max(newZoomScale, scrollView.minimumZoomScale);
        scrollView.setZoomScale(newZoomScale, animated: true)
        centerImageInScrollView()
    }
    
    //MARK: UIViewController changing view size (e.g. orientation)
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        mainImageView?.removeFromSuperview()
        //to properly position selected image in different screen orientations and size
        coordinator.animate(alongsideTransition: { (_) -> Void in
        }) {[weak self] (_) -> Void in
            if let weakSelf = self
            {
                weakSelf.addImageHolder(weakSelf.fullSizeImage);
            }
        }
    }
    
    //MARK: -
    fileprivate func getCurrentImageToProcess() -> UIImage?
    {
        guard let _ = mainImageView?.image else
        {
            return nil;
        }
        
        if let previouslySetImage = self.imageToProcess
        {
            return previouslySetImage;
        }
        
        return self.mainImageView?.image;
        
//        let scrollViewFrame =  scrollView.superview!.convertRect(scrollView.frame, toView:self.view)
//        UIGraphicsBeginImageContext(self.view.bounds.size)//, false, UIScreen.mainScreen().scale)
//        let gContext = UIGraphicsGetCurrentContext()
//        self.view.layer.renderInContext(gContext!)
//        let image = UIGraphicsGetImageFromCurrentImageContext()
//        guard let croppedImageRef = CGImageCreateWithImageInRect(image.CGImage, scrollViewFrame) else
//        {
//            return nil
//        }
//        let croppedImage = UIImage(CGImage: croppedImageRef);
//        
//        UIGraphicsEndImageContext();
//        return croppedImage;
    }
    
    override func setImageToProceed(_ image: UIImage) {
        self.fullSizeImage = image;
        self.addImageHolder(self.fullSizeImage);
    }
    
    //MARK: - JigPixAlgorithm
    func startProcess(_ targetImage:UIImage)
    {
        let sourceImage = self.getPuzzleImageById(puzzleImageIdx);
        self.targetImage = targetImage;
        
        self.algorithm = JigpixAlgoritm();
        self.algorithm?.delegate = self;
        
        let priority = DispatchQoS.QoSClass.userInitiated; //the lower qos class - the slower image processing
        
        DispatchQueue.global(qos: priority).async {[unowned self] in
            self.algorithm?.Start(sourceImage, target: targetImage);
        }
        
        self.coordinateGridView.ShowGrid(true, columns: JigpixAlgoritm.COLS, rows: JigpixAlgoritm.ROWS);
    }
    
    //MARK: - JigPixAlgorithmDelegate
    func jigpixAlgoritmDidStart(_ image: UIImage) {
        
        //if ModeHolder.sharedInstance.colorMode == .BlackWhite
        //{
        //addGridDrawerViewTo(mainImageView)
        //}
        self.targetImage = image;
        convertButton.isEnabled = false;
        scrollView.isUserInteractionEnabled = false;
        //setLoadingIndicatorVisible(true)
    }
    
    func jigpixAlgoritmImageChanged(_ image: UIImage) {
        self.targetImage = image;
    }
    
    func jigpixAlgoritmDidEnd(_ image: UIImage) {
        self.targetImage = image;
        convertButton.isEnabled = true;
        convertButton.setNextTitle();
        scrollView.isUserInteractionEnabled = true;
        //setLoadingIndicatorVisible(false)
        self.algorithm?.delegate = nil;
    }
}

