//
//  UIViewController+Extensions.swift
//  JigsawPuzzleApp
//
//  Created by Ivan Yavorin on 1/8/16.
//  Copyright © 2016 JigSaw. All rights reserved.
//

import UIKit

extension UIViewController
{
    func setLoadingIndicatorVisible(_ visible:Bool)
    {
        if visible
        {
            if let indicator = self.view.viewWithTag(0x70AD) as? UIActivityIndicatorView
            {
                if indicator.isAnimating
                {
                    return //already showing
                }
                else
                {
                    indicator.startAnimating()
                }
                return
            }
            
            let indicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
            indicatorView.tag = 0x70AD
            let frame = CGRect(x: 0, y: 0, width: 200.0, height: 200.0)
            indicatorView.frame = frame
            indicatorView.layer.cornerRadius = 7.0
            indicatorView.backgroundColor = UIColor.black.withAlphaComponent(0.7)
            indicatorView.center = CGPoint(x: self.view.bounds.midX, y: self.view.bounds.midY)
            indicatorView.autoresizingMask =  [.flexibleLeftMargin , .flexibleRightMargin , .flexibleTopMargin , .flexibleBottomMargin]
            self.view.addSubview(indicatorView)
            indicatorView.startAnimating()
        }
        else
        {
            if let indicator = self.view.viewWithTag(0x70AD) as? UIActivityIndicatorView
            {
                indicator.stopAnimating()
            }
        }
    }
}
