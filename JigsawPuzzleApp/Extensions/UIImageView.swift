//
//  UIImageView.swift
//  Jigpix
//
//  Created by Admin on 4/2/16.
//  Copyright © 2016 UNIPUZZLE, INC. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView{
    
    func makeBlurImage()
    {
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight] // for supporting device rotation
        self.addSubview(blurEffectView)
    }
    
}
