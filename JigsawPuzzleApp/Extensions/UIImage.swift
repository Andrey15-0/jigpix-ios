//
//  UIImage.swift
//  JigsawPuzzleApp
//
//  Created by Ivan Yavorin on 12/5/15.
//  Copyright © 2015 JigSaw. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    
    func scaleToSizeKeepAspect(_ newSize:CGSize) -> UIImage?
    {
        UIGraphicsBeginImageContext(newSize)
        defer {
            UIGraphicsEndImageContext()
        }
        var widthRatio:CGFloat = newSize.width / self.size.width
        var heightRatio:CGFloat = newSize.height / self.size.height
        
        if widthRatio > heightRatio
        {
            widthRatio = heightRatio / widthRatio
            heightRatio = 1.0
        }
        else
        {
            heightRatio = widthRatio / heightRatio
            widthRatio = 1.0
        }
        
        if let context:CGContext = UIGraphicsGetCurrentContext()
        {
            context.translateBy(x: 0.0, y: newSize.height)
            context.scaleBy(x: 1.0, y: -1.0)
            let rect = CGRect(x: newSize.width / 2 - (newSize.width * widthRatio) / 2, y: newSize.height / 2 - (newSize.height - heightRatio) / 2, width: newSize.width * widthRatio, height: newSize.height * heightRatio)
            context.draw(self.cgImage!, in: rect)
            
            let scaledImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
            
            //UIGraphicsEndImageContext()
            
            return scaledImage
        }
        
        //UIGraphicsEndImageContext()
        return nil
    }
    
    func fixOrientation() -> UIImage
    {
        let selfOrientation = self.imageOrientation
        
        
        // We need to calculate the proper transformation to make the image upright.
        // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
        
        var transform = CGAffineTransform.identity
        
        switch selfOrientation
        {
        case .up:
            return self
        case .down:
            fallthrough
        case .downMirrored:
            transform = transform.translatedBy(x: self.size.width, y: self.size.height)
            transform = transform.rotated(by: CGFloat(Double.pi))
        case .left:
            fallthrough
        case .leftMirrored:
            transform = transform.translatedBy(x: self.size.width, y: 0)
            transform = transform.rotated(by: CGFloat(Double.pi/2.0))
        case .right:
            fallthrough
        case .rightMirrored:
            transform = transform.translatedBy(x: 0, y: self.size.height)
            transform = transform.rotated(by: CGFloat(-(Double.pi/2.0)))
        default: break
        }
        
        
        switch selfOrientation
        {
        case .upMirrored:
            fallthrough
        case .downMirrored:
            transform = transform.translatedBy(x: self.size.width, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
        case .leftMirrored:
            fallthrough
        case .rightMirrored:
            transform = transform.translatedBy(x: self.size.height, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
        default: break
        }
        
        
        // Now we draw the underlying CGImage into a new context, applying the transform
        // calculated above
        //CGBitmapContextCreate(nil, Int(self.size.width), Int(self.size.height), CGImageGetBitsPerComponent(self.CGImage), 0, CGImageGetColorSpace(self.CGImage), <#T##bitmapInfo: UInt32##UInt32#>)
        
        if let context = CGContext(data: nil, width: Int(self.size.width * ceil(self.scale)), height: Int(self.size.height * ceil(self.scale)), bitsPerComponent: (self.cgImage?.bitsPerComponent)!, bytesPerRow: 0, space: (self.cgImage?.colorSpace!)!, bitmapInfo: (self.cgImage?.bitmapInfo.rawValue)!)
        {
            
            context.concatenate(transform)
            
            switch selfOrientation
            {
            case .left:
                fallthrough
            case .leftMirrored:
                fallthrough
            case .right:
                fallthrough
            case .rightMirrored:
                context.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: self.size.height, height: self.size.width)) //once again.. switch
            default:
                context.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height))
            }
            
            // And now we just create a new UIImage from the drawing context
            
            if let cgImageLV = context.makeImage()
            {
                let toReturnFixed = UIImage(cgImage: cgImageLV)
                //                if toReturnFixed != nil
                //                {
                return toReturnFixed
                //                }
                
            }
            
            // if By some reason could not create image
            return self;
        }
        
        return self
    }
    
    func croptoRect(_ rect:CGRect) -> UIImage?
    {
        var lvRect = rect
        if (self.scale > 1.0)
        {
            lvRect = CGRect(x: rect.origin.x * self.scale,
                            y: rect.origin.y * self.scale,
                            width: rect.size.width * self.scale,
                            height: rect.size.height * self.scale);
        }
        
        if let imageRef = self.cgImage?.cropping(to: lvRect)
        {
            let result =  UIImage(cgImage: imageRef, scale: self.scale, orientation: self.imageOrientation)//[UIImage imageWithCGImage:imageRef scale:self.scale orientation:self.imageOrientation];
            //CGImageRelease(imageRef);
            return result;
        }
        
        return nil
    }
    
    func roundCorners() -> UIImage
    {
        // begin a new image
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale);
        //create a rectangle
        let bounds = CGRect(origin: CGPoint(x: 0, y: 0), size: self.size);
        // add a clip in the shape of a rounded rectangle
        UIBezierPath(roundedRect: bounds, cornerRadius: 10.0).addClip();
        // draw the image in the view
        self.draw(in: bounds);
        // set the image
        let image = UIGraphicsGetImageFromCurrentImageContext();
        // clean up
        UIGraphicsEndImageContext();
        
        return image!;
    }
}
