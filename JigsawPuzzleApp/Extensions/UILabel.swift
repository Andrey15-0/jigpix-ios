//
//  UILabel.swift
//  Jigpix
//
//  Created by Admin on 3/17/16.
//  Copyright © 2016 UNIPUZZLE, INC. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    var substituteFontName : String {
        get { return self.font.fontName }
        set { self.font = UIFont(name: newValue, size: self.font.pointSize) }
    }
}
