//
//  Enums.swift
//  JigsawPuzzleApp
//
//  Created by CloudCraft on 1/6/16.
//  Copyright © 2016 JigSaw. All rights reserved.
//

import Foundation

enum ConvertButtonState{
    case convert(title:String)
    case converting(title:String)
    case getCode(title:String)
    case done(title:String)
}
