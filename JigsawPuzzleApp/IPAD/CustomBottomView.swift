//
//  CustomBottomView.swift
//  TestConstraints
//
//  Created by Admin on 4/10/16.
//  Copyright © 2016 unipuzzle. All rights reserved.
//

import Foundation
import UIKit

//MARK: - Protocol JigpixAlgoritmDelegate
protocol CustomBottomViewDelegate {
    func pieceCountChanged();
    func backButtonTapped(_ sender: UIButton);
    func actionButtonTapped(_ sender: UIButton);
    func cameraButtonTapped(_ sender: UIButton);
    func infoButtonTapped(_ sender: UIButton);
    func onStateChanged(_ state: ConvertStates);
}

class CustomConvertButton: UIButton
{
    //    override func drawRect(rect: CGRect) {
    //        //1 - get the current context
    //        let context = UIGraphicsGetCurrentContext();
    //        var colors: [CGColor] = [];
    //        colors.append(UIColor(red: 226.0/255.0, green: 1.0/255.0, blue: 127.0/255.0, alpha: 1.0).CGColor);
    //        colors.append(UIColor(red: 234.0/255.0, green: 89.0/255.0, blue: 16.0/255.0, alpha: 1.0).CGColor);
    //        colors.append(UIColor(red: 190.0/255.0, green: 217.0/255.0, blue: 65.0/255.0, alpha: 1.0).CGColor);
    //        colors.append(UIColor(red: 21.0/255.0, green: 164.0/255.0, blue: 229.0/255.0, alpha: 1.0).CGColor);
    //        colors.append(UIColor(red: 14.0/255.0, green: 99.0/255.0, blue: 51.0/255.0, alpha: 1.0).CGColor);
    //        colors.append(UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0).CGColor);
    //
    //        //2 - set up the color space
    //        let colorSpace = CGColorSpaceCreateDeviceRGB();
    //
    //        //3 - set up the color stops
    //        let colorLocations:[CGFloat] = [0.0, 0.2, 0.4, 0.6, 0.8, 1.0];
    //
    //        //4 - create the gradient
    //        let gradient = CGGradientCreateWithColors(colorSpace,
    //                                                  colors,
    //                                                  colorLocations);
    //
    //        let clipPath = UIBezierPath(roundedRect: rect, cornerRadius: 8.0);
    //        clipPath.addClip();
    //
    //        //5 - draw the gradient
    //        let startPoint = CGPoint.zero;
    //        let endPoint = CGPoint(x:self.bounds.width, y:0);
    //        CGContextDrawLinearGradient(context,
    //                                    gradient,
    //                                    startPoint,
    //                                    endPoint,
    //                                    CGGradientDrawingOptions.DrawsAfterEndLocation);
    //    }
    
    override func layoutSubviews()
    {
        self.layer.cornerRadius = 8.0;
        self.layer.borderColor = UIColor.white.cgColor;
        //self.layer.borderWidth = 2;
        
        super.layoutSubviews()
    }
    
    internal func setTitleForState(_ state:ConvertStates)
    {
        var title = "";
        
        switch state
        {
        case .convert:
            title = "Convert";
        case .converting:
            title = "Converting";
        case .getCode:
            title = "Get Code";
        case .done:
            title = "Done";
        }//end switch
        
        let attrText = FlexibleTitleButton.convertStringToAttributedColoredText(title);
        self.setAttributedTitle(attrText, for: UIControlState());
        //        self.convertButton.setTitle(title, forState: UIControlState.Normal);
        if(state == .done)
        {
            self.layer.borderWidth = 0;
        }
        else
        {
            self.layer.borderWidth = 2;
        }
    }
}

class CustomBottomView: UIView
{
    @IBOutlet weak var convertButton:CustomConvertButton!;
    @IBOutlet weak var tabBarView:UIView!;
    @IBOutlet weak var backButton:UIButton!;
    @IBOutlet weak var cameraButton:UIButton!;
    @IBOutlet weak var actionButton:UIButton!;
    @IBOutlet weak var infoButton:UIButton!;
    
    @IBOutlet weak var puzzlePieceQuantityIndicator:UIImageView!;
    @IBOutlet weak var leftImageView:UIImageView!;
    @IBOutlet weak var rightImageView:UIImageView!;
    
    var delegate: CustomBottomViewDelegate? = nil;
    
    var currentState: ConvertStates = ConvertStates.convert;
    
    var leftImage:UIImage = UIImage() {
        didSet{
            if leftImage.size.width > 0
            {
                self.leftImageView.image = self.leftImage;
                self.leftImageView.alpha = 0.0;
                UIView.animate(withDuration: 0.5, animations: {[unowned self] in
                    self.leftImageView.alpha = 1.0;
                    });
            }
            else
            {
                leftImageView.image = nil;
            }
        }
    }
    
    var rightImage:UIImage = UIImage() {
        didSet{
            if rightImage.size.width > 0
            {
                self.rightImageView.image = self.rightImage;
                self.rightImageView.alpha = 0.0;
                UIView.animate(withDuration: 0.5, animations: {[unowned self] in
                    self.rightImageView.alpha = 1.0;
                    });
            }
            else
            {
                rightImageView.image = nil;
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame);
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
    }
    
    override func awakeAfter(using aDecoder: NSCoder) -> Any? {
        if self.subviews.count == 0 {
            return self.loadNib();
        }
        return self;
    }
    
    fileprivate func loadNib() -> CustomBottomView {
        return Bundle.main.loadNibNamed("CustomBottomView", owner: nil, options: nil)![0] as! CustomBottomView
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if touches.count == 1 && self.convertButton.isEnabled {
            if let touch = touches.first {
                let touchLocation = touch.location(in: self);
                
                let leftImageViewFrame = self.convert(self.leftImageView.frame, from: self.leftImageView.superview);
                // Check if the touch is inside the left view
                if leftImageViewFrame.contains(touchLocation) {
                    self.changeState(.convert);
                }
                
                let rightImageViewFrame = self.convert(self.rightImageView.frame, from: self.rightImageView.superview);
                // Check if the touch is inside the right view
                if rightImageViewFrame.contains(touchLocation) {
                    self.changeState(.getCode);
                }
            }
        }
    }
    
    internal func sizeChanged(_ size: CGSize)
    {
        self.convertButtonRefresh();
        let medium = UIImage(named: "520_icon.png")!.withRenderingMode(UIImageRenderingMode.alwaysTemplate);
        let small = UIImage(named: "300_icon.png")!.withRenderingMode(UIImageRenderingMode.alwaysTemplate);
        self.puzzlePieceQuantityIndicator.image =  JigpixAlgoritm.puzzlePieces == Pieces.medium520 ? medium : small;
        self.puzzlePieceQuantityIndicator.tintColor = UIColor.white;
        
        //self.tabBarView.addBorder(edges: UIRectEdge.Top, colour: UIColor.whiteColor(), thickness: 2);
        
        let isLandscape = size.width > size.height;
        
        self.backgroundColor = UIColor.clear;
        
        self.applyConvertButtonConstraints(isLandscape);
    }
    
    fileprivate func applyConvertButtonConstraints(_ isLandscape: Bool)
    {
        self.convertButton.removeConstraints(self.convertButton.constraints);
        
        let superview = self.convertButton.superview!;
        for c: NSLayoutConstraint in superview.constraints {
            if (c.firstItem.isEqual(self.convertButton) || self.convertButton.isEqual(c.secondItem)) {
                superview.removeConstraint(c);
            }
        }
        
        if isLandscape
        {
            switch self.currentState
            {
            case .converting, .getCode:
                let leftConstraint = NSLayoutConstraint(item: superview, attribute: NSLayoutAttribute.left, relatedBy: NSLayoutRelation.equal, toItem: self.convertButton, attribute: NSLayoutAttribute.left, multiplier: 1, constant: 0);
                superview.addConstraint(leftConstraint);
                
                let rightConstraint = NSLayoutConstraint(item: superview, attribute: NSLayoutAttribute.right, relatedBy: NSLayoutRelation.equal, toItem: self.convertButton, attribute: NSLayoutAttribute.right, multiplier: 1, constant: 0);
                superview.addConstraint(rightConstraint);
                
                let bottomConstraint = NSLayoutConstraint(item: superview, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: self.convertButton, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: 0);
                superview.addConstraint(bottomConstraint);
                
                let aspectConstraint = NSLayoutConstraint(item: self.convertButton, attribute: .width, relatedBy: .equal, toItem: self.convertButton, attribute: .height, multiplier: 5.0/2.0, constant: 0.0);
                self.convertButton.addConstraint(aspectConstraint);
                break;
            case .done:
                let leftConstraint = NSLayoutConstraint(item: superview, attribute: NSLayoutAttribute.left, relatedBy: NSLayoutRelation.equal, toItem: self.convertButton, attribute: NSLayoutAttribute.left, multiplier: 1, constant: 0);
                superview.addConstraint(leftConstraint);
                
                let rightConstraint = NSLayoutConstraint(item: superview, attribute: NSLayoutAttribute.right, relatedBy: NSLayoutRelation.equal, toItem: self.convertButton, attribute: NSLayoutAttribute.right, multiplier: 1, constant: 0);
                superview.addConstraint(rightConstraint);
                
                let aspectConstraint = NSLayoutConstraint(item: self.convertButton, attribute: .width, relatedBy: .equal, toItem: self.convertButton, attribute: .height, multiplier: 10.0/2.0, constant: 0.0);
                self.convertButton.addConstraint(aspectConstraint);
                
                let convertButtonYConstraint = NSLayoutConstraint(item: superview, attribute: NSLayoutAttribute.centerY, relatedBy: NSLayoutRelation.equal, toItem: self.convertButton, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0);
                superview.addConstraint(convertButtonYConstraint);
                break;
            default:
                let leftConstraint = NSLayoutConstraint(item: superview, attribute: NSLayoutAttribute.left, relatedBy: NSLayoutRelation.equal, toItem: self.convertButton, attribute: NSLayoutAttribute.left, multiplier: 1, constant: 0);
                superview.addConstraint(leftConstraint);
                
                let rightConstraint = NSLayoutConstraint(item: superview, attribute: NSLayoutAttribute.right, relatedBy: NSLayoutRelation.equal, toItem: self.convertButton, attribute: NSLayoutAttribute.right, multiplier: 1, constant: 0);
                superview.addConstraint(rightConstraint);
                
                let aspectConstraint = NSLayoutConstraint(item: self.convertButton, attribute: .width, relatedBy: .equal, toItem: self.convertButton, attribute: .height, multiplier: 5.0/2.0, constant: 0.0);
                self.convertButton.addConstraint(aspectConstraint);
                
                let convertButtonYConstraint = NSLayoutConstraint(item: superview, attribute: NSLayoutAttribute.centerY, relatedBy: NSLayoutRelation.equal, toItem: self.convertButton, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0);
                superview.addConstraint(convertButtonYConstraint);
                break;
            }//end switch
        }
        else
        {
            let topConstraint = NSLayoutConstraint(item: superview, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: self.convertButton, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0);
            superview.addConstraint(topConstraint);
            
            let leftConstraint = NSLayoutConstraint(item: superview, attribute: NSLayoutAttribute.left, relatedBy: NSLayoutRelation.equal, toItem: self.convertButton, attribute: NSLayoutAttribute.left, multiplier: 1, constant: 0);
            superview.addConstraint(leftConstraint);
            
            let rightConstraint = NSLayoutConstraint(item: superview, attribute: NSLayoutAttribute.right, relatedBy: NSLayoutRelation.equal, toItem: self.convertButton, attribute: NSLayoutAttribute.right, multiplier: 1, constant: 0);
            superview.addConstraint(rightConstraint);
            
            let bottomConstraint = NSLayoutConstraint(item: superview, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: self.convertButton, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: 0);
            superview.addConstraint(bottomConstraint);
        }
        
        self.applyLeftImageViewConstraints(isLandscape);
        self.applyRightImageViewConstraints(isLandscape);
    }
    
    fileprivate func applyLeftImageViewConstraints(_ isLandscape: Bool)
    {
        self.leftImageView.removeConstraints(self.leftImageView.constraints);
        
        let superview = self.leftImageView.superview!;
        for c: NSLayoutConstraint in superview.constraints {
            if (c.firstItem.isEqual(self.leftImageView) || self.leftImageView.isEqual(c.secondItem)) {
                superview.removeConstraint(c);
            }
        }
        
        if isLandscape
        {
            self.leftImageView.isHidden = self.currentState == .convert ? true : false;
            
            let aspectConstraint = NSLayoutConstraint(item: self.leftImageView, attribute: .width, relatedBy: .equal, toItem: self.leftImageView, attribute: .height, multiplier: JigpixAlgoritm.puzzlePieces == Pieces.medium520 ? 10.0/13.0 : 3.0/4.0, constant: 0);
            self.leftImageView.addConstraint(aspectConstraint);
            
            let topConstraint = NSLayoutConstraint(item: superview, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: self.leftImageView, attribute: NSLayoutAttribute.top, multiplier: 1, constant: -1);
            superview.addConstraint(topConstraint);
            
            let bottomConstraint = NSLayoutConstraint(item: self.convertButton, attribute: NSLayoutAttribute.topMargin, relatedBy: NSLayoutRelation.equal, toItem: self.leftImageView, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: 20);
            superview.addConstraint(bottomConstraint);
            
            let centerXConstraint = NSLayoutConstraint(item: superview, attribute: NSLayoutAttribute.centerX, relatedBy: NSLayoutRelation.equal, toItem: self.leftImageView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0);
            superview.addConstraint(centerXConstraint);
        }
        else
        {
            self.leftImageView.isHidden = true;
        }
    }
    
    fileprivate func applyRightImageViewConstraints(_ isLandscape: Bool)
    {
        self.rightImageView.removeConstraints(self.rightImageView.constraints);
        
        let superview = self.rightImageView.superview!;
        for c: NSLayoutConstraint in superview.constraints {
            if (c.firstItem.isEqual(self.rightImageView) || self.rightImageView.isEqual(c.secondItem)) {
                superview.removeConstraint(c);
            }
        }
        
        if isLandscape
        {
            self.rightImageView.isHidden = self.currentState == .done ? false : true;
            
            let aspectConstraint = NSLayoutConstraint(item: self.rightImageView, attribute: .width, relatedBy: .equal, toItem: self.rightImageView, attribute: .height, multiplier: JigpixAlgoritm.puzzlePieces == Pieces.medium520 ? 10.0/13.0 : 3.0/4.0, constant: 0.0);
            self.rightImageView.addConstraint(aspectConstraint);
            
            let topConstraint = NSLayoutConstraint(item: self.convertButton, attribute: NSLayoutAttribute.bottomMargin, relatedBy: NSLayoutRelation.equal, toItem: self.rightImageView, attribute: NSLayoutAttribute.top, multiplier: 1, constant: -20);
            superview.addConstraint(topConstraint);
            
            let bottomConstraint = NSLayoutConstraint(item: superview, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: self.rightImageView, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: 0);
            superview.addConstraint(bottomConstraint);
            
            let centerXConstraint = NSLayoutConstraint(item: superview, attribute: NSLayoutAttribute.centerX, relatedBy: NSLayoutRelation.equal, toItem: self.rightImageView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0);
            superview.addConstraint(centerXConstraint);
        }
        else
        {
            self.rightImageView.isHidden = true;
        }
    }
    
    fileprivate func convertButtonRefresh()
    {
        self.convertButton.backgroundColor = UIColor.clear;
        
        self.convertButton.titleLabel!.textAlignment = NSTextAlignment.center;
        self.convertButton.titleLabel!.numberOfLines = 0;
        self.convertButton.titleLabel!.adjustsFontSizeToFitWidth = true;
        self.convertButton.setTitleForState(.convert);
        
        self.leftImageView.layer.cornerRadius = 8.0;
        self.leftImageView.layer.borderColor = UIColor.white.cgColor;
        self.leftImageView.layer.borderWidth = 2;
        self.leftImageView.clipsToBounds = true;
        
        self.rightImageView.layer.cornerRadius = 8.0;
        self.rightImageView.layer.borderColor = UIColor.white.cgColor;
        self.rightImageView.layer.borderWidth = 2;
        self.rightImageView.clipsToBounds = true;
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton)
    {
        if delegate != nil
        {
            self.delegate!.backButtonTapped(sender);
        }
    }
    
    @IBAction func actionButtonTapped(_ sender: UIButton)
    {
        if delegate != nil
        {
            self.delegate!.actionButtonTapped(sender);
        }
    }
    
    @IBAction func cameraButtonTapped(_ sender: UIButton)
    {
        if delegate != nil
        {
            self.delegate!.cameraButtonTapped(sender);
        }
    }
    
    @IBAction func infoButtonTapped(_ sender: UIButton)
    {
        if delegate != nil
        {
            self.delegate!.infoButtonTapped(sender);
        }
    }
    
    @IBAction func convertButtonTapped(_ sender: UIButton)
    {
        var convertState:ConvertStates = .convert;
        
        switch self.currentState
        {
        case .convert:
            convertState = .converting;
        case .converting:
            convertState = .getCode;
        case .getCode:
            convertState = .done;
        case .done:
            convertState = .convert;
        }//end switch
        
        self.changeState(convertState);
    }
    
    fileprivate func changeState(_ state: ConvertStates)
    {
        self.onStateChanged(state);
        
        if delegate != nil
        {
            self.delegate!.onStateChanged(state);
        }
    }
    
    fileprivate func onStateChanged(_ state: ConvertStates)
    {
        self.currentState = state;

        self.convertButton.setTitleForState(state);
        
        self.animateConstraints();
    }
    
    fileprivate func animateConstraints()
    {
        let isLandscape = self.bounds.size.width < self.bounds.size.height;
        self.applyConvertButtonConstraints(isLandscape);
        
        //        let superview = self.convertButton.superview!;
        //        superview.removeConstraint(self.bottomConstraintConvertButton);
        //        self.bottomConstraintConvertButton = NSLayoutConstraint(item: superview, attribute: NSLayoutAttribute.Bottom, relatedBy: NSLayoutRelation.Equal, toItem: self.convertButton, attribute: NSLayoutAttribute.Bottom, multiplier: 1, constant: 0);
        //        superview.addConstraint(self.bottomConstraintConvertButton);
    }
    
    internal func disableTabBar()
    {
        self.convertButton.isEnabled = false;
        
        self.cameraButton.isEnabled = false;
        self.actionButton.isEnabled = false;
        self.infoButton.isEnabled = false;
    }
    
    internal func enableTabBar()
    {
        self.convertButton.isEnabled = true;
        
        self.cameraButton.isEnabled = true;
        self.actionButton.isEnabled = true;
        self.infoButton.isEnabled = true;
    }
}

enum ConvertStates{
    case convert
    case converting
    case getCode
    case done
}
