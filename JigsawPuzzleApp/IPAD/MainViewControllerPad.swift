//
//  ViewController.swift
//  TestConstraints
//
//  Created by Admin on 4/5/16.
//  Copyright © 2016 unipuzzle. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class MainViewControllerPad: ActionsViewController, CustomBottomViewDelegate, UIScrollViewDelegate, JigpixAlgoritmDelegate {
    
    internal var originalImage:UIImage?;
    internal var puzzleImageIdx:Int = 0;
    
    @IBOutlet weak var coordinateView:CoordinateGridView!;
    @IBOutlet weak var scrollView:UIScrollView!;
    @IBOutlet weak var aspectConstraintCoordinateGrid:NSLayoutConstraint!;
    @IBOutlet weak var aspectConstraintScrollView:NSLayoutConstraint!;
    
    fileprivate var bottomView:CustomBottomView?;
    
    fileprivate lazy var algorithm = JigpixAlgoritm();
    
    fileprivate var mainImageView:UIImageView?;
    
    fileprivate var doubleTapRecognizer:UITapGestureRecognizer?;
    
    override func viewDidLoad() {
        super.viewDidLoad();
        // Do any additional setup after loading the view, typically from a nib.
        
        // by double tapping on image - in becomes smaller a little bit and centers itself in scrollView
        self.doubleTapRecognizer = UITapGestureRecognizer (target: self, action: #selector(MainSceneViewController.doubleTapAction(_:)));
        
        self.algorithm.delegate = self;
        
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.light);
        let blurEffectView = UIVisualEffectView(effect: blurEffect);
        blurEffectView.frame = self.view.bounds;
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]; // for supporting device rotation
        self.view.insertSubview(blurEffectView, at: 0);
        
        let tmp = 1000;
        let bv = CustomBottomView.loadFromNibNamed("CustomBottomView") as! CustomBottomView;
        bv.frame = CGRect(x: tmp, y: tmp, width: tmp, height: tmp);
        bv.translatesAutoresizingMaskIntoConstraints = false;
        bv.isHidden = true;
        self.bottomView = bv;
        let puzzleImage = self.getPuzzleImageById(puzzleImageIdx).rotate(90);
        self.bottomView!.backButton.setImage(puzzleImage, for: UIControlState());
        self.bottomView!.backButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.fill;
        self.bottomView!.backButton.contentVerticalAlignment = UIControlContentVerticalAlignment.fill;
        self.bottomView!.backButton.imageEdgeInsets = UIEdgeInsetsMake(4,0,-8,0);
        self.bottomView!.backButton.imageView?.contentMode = .scaleToFill;
        self.bottomView?.delegate = self;
        self.view.addSubview(bv);
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning();
        // Dispose of any resources that can be recreated.
        
        self.algorithm.stopWorking = true;
        if(self.bottomView?.currentState == .converting)
        {
            self.showMemoryOutOfMemoryMessage();
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
        
        let backgroundImage = self.originalImage!.resizeImage2(self.view.bounds.size);
        self.view.backgroundColor = UIColor(patternImage: backgroundImage);
        
        self.rotated(self.view.bounds.size);
        
        self.bottomView?.isHidden = false;
    }
    
    override var prefersStatusBarHidden : Bool {
        return true;
    }
    
    //MARK: - CustomBottomViewDelegate
    func actionButtonTapped(_ sender: UIButton)
    {
        let outputImage = createImageForSaving();
        showActionsDialog(sender, image: outputImage);
    }
    
    func createImageForSaving() -> UIImage
    {
        if(self.bottomView?.currentState == ConvertStates.done)
        {
            let logo = UIImage(named: "Logo_Final")!;
            let puzzle = self.getPuzzleImageById(puzzleImageIdx).roundCorners();
            let selfie = self.bottomView!.leftImage.roundCorners();
            let result = self.bottomView!.rightImage.roundCorners();
            
            UIGraphicsBeginImageContextWithOptions(self.coordinateView.bounds.size, false, 0);
            self.coordinateView.drawHierarchy(in: self.coordinateView.bounds, afterScreenUpdates: true);
            var image:UIImage = UIGraphicsGetImageFromCurrentImageContext()!;
            UIGraphicsEndImageContext();
            image = image.resizeImage2(CGSize(width: 1120.0, height: 1408.0));
            
            let heightOffset = image.size.height / 5.0;
            let sizeResult = CGSize(width: image.size.width, height: image.size.height + heightOffset);
            let opaque = false;
            let scale: CGFloat = 0;
            UIGraphicsBeginImageContextWithOptions(sizeResult, opaque, scale);
            let context = UIGraphicsGetCurrentContext();
            
            let borderRect = CGRect(x: 0, y: 0, width: sizeResult.width, height: sizeResult.height);
            context?.setFillColor(UIColor.gray.cgColor);
            context?.addRect(borderRect);
            context?.drawPath(using: CGPathDrawingMode.fill);
            
            let border: CGFloat = 40.0;
            let mainRect = CGRect(x: border, y: border, width: sizeResult.width - 2.0 * border, height: sizeResult.height - 2.0 * border);
            context?.setFillColor(UIColor.white.cgColor);
            context?.addRect(mainRect);
            context?.drawPath(using: CGPathDrawingMode.fill);
            
            let yOffset: CGFloat = 5.0;
            //Draw logo
            let logoRect = CGRect(x: border, y: yOffset + border + 10, width: image.size.width / 2.5 - border, height: heightOffset - yOffset - 30);
            logo.draw(in: logoRect);
            
            let smallImageWidth: CGFloat = (image.size.width - image.size.width / 2.5) / 3.0 - 40;
            
            //Draw puzzle
            let puzzleRect = CGRect(x: image.size.width / 2.5, y: yOffset + border, width: smallImageWidth, height: heightOffset - yOffset);
            puzzle.draw(in: puzzleRect);
            
            context?.setLineWidth(3.0);
            context?.setStrokeColor(UIColor.red.cgColor);
            
            //Draw plus
            context?.move(to: CGPoint(x: puzzleRect.origin.x + smallImageWidth + 2, y: heightOffset / 2.0 + border));
            context?.addLine(to: CGPoint(x: puzzleRect.origin.x + smallImageWidth + 2 + 20, y: heightOffset / 2.0 + border));
            context?.strokePath();
            context?.move(to: CGPoint(x: puzzleRect.origin.x + smallImageWidth + 2 + 10, y: heightOffset / 2.0 + border - 10));
            context?.addLine(to: CGPoint(x: puzzleRect.origin.x + smallImageWidth + 2 + 10, y: heightOffset / 2.0 + border + 10));
            context?.strokePath();
            
            //Draw selfie
            let selfieRect = CGRect(x: image.size.width / 2.5 + smallImageWidth + 2 + 20 + 2, y: puzzleRect.origin.y, width: smallImageWidth, height: heightOffset - yOffset);
            selfie.draw(in: selfieRect);
            
            //Draw equal
            context?.move(to: CGPoint(x: selfieRect.origin.x + smallImageWidth + 2, y: heightOffset / 2.0 - 8 + border));
            context?.addLine(to: CGPoint(x: selfieRect.origin.x + smallImageWidth + 2 + 20, y: heightOffset / 2.0 - 8 + border));
            context?.strokePath();
            context?.move(to: CGPoint(x: selfieRect.origin.x + smallImageWidth + 2, y: heightOffset / 2.0 + 4 + border));
            context?.addLine(to: CGPoint(x: selfieRect.origin.x + smallImageWidth + 2 + 20, y: heightOffset / 2.0 + 4 + border));
            context?.strokePath();
            
            //Draw result
            let resultRect = CGRect(x: selfieRect.origin.x + smallImageWidth + 2 + 20 + 2, y: puzzleRect.origin.y, width: smallImageWidth, height: heightOffset - yOffset);
            result.draw(in: resultRect);
            
            //Draw image
            let imageRect = CGRect(x: border, y: heightOffset + border, width: image.size.width - 2.0 * border, height: image.size.height - 2.0 * border);
            image.draw(in: imageRect);
            
            let outputImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            return outputImage!;
        }
        else
        {
            UIGraphicsBeginImageContextWithOptions(self.coordinateView.bounds.size, false, 0);
            self.coordinateView.drawHierarchy(in: self.coordinateView.bounds, afterScreenUpdates: true);
            var image:UIImage = UIGraphicsGetImageFromCurrentImageContext()!;
            UIGraphicsEndImageContext();
            image = image.resizeImage2(CGSize(width: 1120.0, height: 1408.0));
            
            return image;
        }
    }
    
    func backButtonTapped(_ sender: UIButton)
    {
        self.algorithm.stopWorking = true;
        self.dismiss(animated: true, completion: nil);
    }
    
    func cameraButtonTapped(_ sender: UIButton)
    {
        showCameraDialog(sender);
    }
    
    func infoButtonTapped(_ sender: UIButton)
    {
        showInfoDialog(sender);
    }
    
    func pieceCountChanged()
    {
        self.aspectChange();
    }
    
    func onStateChanged(_ state: ConvertStates)
    {
        switch state
        {
        case .convert:
            self.addImageHolder(self.originalImage);
            self.coordinateView.ShowGrid(false, columns: JigpixAlgoritm.COLS, rows: JigpixAlgoritm.ROWS);
            self.bottomView?.enableTabBar();
            self.bottomView?.leftImage = UIImage();
            self.bottomView?.rightImage = UIImage();
            break;
        case .converting:
            if let imageToProcess = self.getCurrentImageToProcess()
            {
                self.bottomView?.leftImage = imageToProcess;
                self.addImageHolder(imageToProcess);
                self.startProcess(imageToProcess);
                self.scrollView.isUserInteractionEnabled = false;
            }
            self.bottomView?.disableTabBar();
            break;
        case .getCode:
            self.scrollView.isUserInteractionEnabled = true;
            self.bottomView?.enableTabBar();
            if(self.bottomView?.rightImage.size.width > 0)
            {
                self.rotateMainImageViewWith(self.bottomView!.rightImage);
            }
            self.bottomView?.rightImage = UIImage();
            break;
        case .done:
            self.bottomView?.rightImage = (self.mainImageView?.image)!;
            self.rotateMainImageView();
            self.bottomView?.enableTabBar();
            break;
        }//end switch
    }
    
    //MARK: UIViewController changing view size (e.g. orientation)
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator);
        self.rotated(size);
    }
    
    fileprivate func aspectChange()
    {
        self.coordinateView.removeConstraint(self.aspectConstraintCoordinateGrid);
        self.aspectConstraintCoordinateGrid = NSLayoutConstraint(item: self.coordinateView, attribute: .width, relatedBy: .equal, toItem: self.coordinateView, attribute: .height, multiplier: JigpixAlgoritm.puzzlePieces == Pieces.medium520 ? 11.0/14.0 : 17.0/22.0, constant: 0.0);
        self.coordinateView.addConstraint(self.aspectConstraintCoordinateGrid);
        
        self.scrollView.removeConstraint(self.aspectConstraintScrollView);
        self.aspectConstraintScrollView = NSLayoutConstraint(item: self.scrollView, attribute: .width, relatedBy: .equal, toItem: self.scrollView, attribute: .height, multiplier: JigpixAlgoritm.puzzlePieces == Pieces.medium520 ? 10.0/13.0 : 3.0/4.0, constant: 0.0);
        self.scrollView.addConstraint(self.aspectConstraintScrollView);
        
        UIView.animate(withDuration: 0.4, animations: {
            self.view.layoutIfNeeded();
            }, completion: {
                (value: Bool) in
                if self.bottomView?.currentState == ConvertStates.convert
                {
                    self.addImageHolder(self.originalImage);
                    self.coordinateView.ShowGrid(false, columns: JigpixAlgoritm.COLS, rows: JigpixAlgoritm.ROWS);
                }
        })
    }
    
    func rotated(_ size: CGSize)
    {
        self.aspectChange();
        
        let bv = self.bottomView!;
        
        let superview = bv.superview!;
        for c: NSLayoutConstraint in superview.constraints {
            if ((c.firstItem.isEqual(bv) || bv.isEqual(c.secondItem)) ||
                (c.firstItem.isEqual(coordinateView) || coordinateView.isEqual(c.secondItem))) {
                superview.removeConstraint(c);
            }
        }
        
        let isLandscape = size.width > size.height;
        let height: CGFloat = isLandscape ? size.height : 150.0;
        
        bv.sizeChanged(size);
        
        let heighConstraints = bv.constraints.filter{ $0.identifier == "Height" }
        bv.removeConstraints(heighConstraints);
        
        if isLandscape
        {
            let topConstraint = NSLayoutConstraint(item: self.view, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: bv, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0);
            self.view.addConstraint(topConstraint);
            
            let trailingConstraint = NSLayoutConstraint(item: self.view, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: bv, attribute: NSLayoutAttribute.trailing, multiplier: 1, constant: 0);
            self.view.addConstraint(trailingConstraint);
            
            let bottomConstraint = NSLayoutConstraint(item: self.view, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: bv, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: 0);
            self.view.addConstraint(bottomConstraint);
            
            let leadingConstraint = NSLayoutConstraint(item: bv, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: self.coordinateView, attribute: NSLayoutAttribute.trailing, multiplier: 1, constant: 0);
            self.view.addConstraint(leadingConstraint);
            
            self.applyVerticalCoordinateViewConstraints();
        }
        else
        {
            let heightConstraint = NSLayoutConstraint(item: bv, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: height);
            heightConstraint.identifier = "Height";
            bv.addConstraint(heightConstraint);
            
            let trailingConstraint = NSLayoutConstraint(item: self.view, attribute: NSLayoutAttribute.trailingMargin, relatedBy: NSLayoutRelation.equal, toItem: bv, attribute: NSLayoutAttribute.trailing, multiplier: 1, constant: -20);
            self.view.addConstraint(trailingConstraint);
            
            let bottomConstraint = NSLayoutConstraint(item: self.view, attribute: NSLayoutAttribute.bottomMargin, relatedBy: NSLayoutRelation.equal, toItem: bv, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: 0);
            self.view.addConstraint(bottomConstraint);
            
            let leadingConstraint = NSLayoutConstraint(item: bv, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: self.view, attribute: NSLayoutAttribute.leadingMargin, multiplier: 1, constant: -20);
            self.view.addConstraint(leadingConstraint);
            
            self.applyHorizontalCoordinateViewConstraints();
        }
        
        //self.view.layoutIfNeeded();
    }
    
    fileprivate func applyVerticalCoordinateViewConstraints()
    {
        let sizeConst = CGFloat(10.0);
        
        let topConstraint = NSLayoutConstraint(item: self.view, attribute: NSLayoutAttribute.topMargin, relatedBy: NSLayoutRelation.equal, toItem: self.coordinateView, attribute: NSLayoutAttribute.top, multiplier: 1, constant: -sizeConst);
        self.view.addConstraint(topConstraint);
        
        let leadingConstraint = NSLayoutConstraint(item: self.view, attribute: NSLayoutAttribute.leadingMargin, relatedBy: NSLayoutRelation.equal, toItem: self.coordinateView, attribute: NSLayoutAttribute.leading, multiplier: 1, constant: sizeConst);
        self.view.addConstraint(leadingConstraint);
        
        let trailingConstraint = NSLayoutConstraint(item: self.view, attribute: NSLayoutAttribute.trailingMargin, relatedBy: NSLayoutRelation.greaterThanOrEqual, toItem: self.coordinateView, attribute: NSLayoutAttribute.trailing, multiplier: 1, constant: -sizeConst);
        trailingConstraint.priority = 999;
        self.view.addConstraint(trailingConstraint);
        
        let bottomConstraint = NSLayoutConstraint(item: self.view, attribute: NSLayoutAttribute.bottomMargin, relatedBy: NSLayoutRelation.equal, toItem: self.coordinateView, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: sizeConst);
        self.view.addConstraint(bottomConstraint);
    }
    
    fileprivate func applyHorizontalCoordinateViewConstraints()
    {
        let sizeConst = CGFloat(-8.0);
        
        let topConstraint = NSLayoutConstraint(item: self.view, attribute: NSLayoutAttribute.topMargin, relatedBy: NSLayoutRelation.equal, toItem: self.coordinateView, attribute: NSLayoutAttribute.top, multiplier: 1, constant: sizeConst);
        self.view.addConstraint(topConstraint);
        
        let leadingConstraint = NSLayoutConstraint(item: self.view, attribute: NSLayoutAttribute.leadingMargin, relatedBy: NSLayoutRelation.greaterThanOrEqual, toItem: self.coordinateView, attribute: NSLayoutAttribute.leading, multiplier: 1, constant: sizeConst);
        leadingConstraint.priority = 999;
        self.view.addConstraint(leadingConstraint);
        
        let trailingConstraint = NSLayoutConstraint(item: self.view, attribute: NSLayoutAttribute.trailingMargin, relatedBy: NSLayoutRelation.greaterThanOrEqual, toItem: self.coordinateView, attribute: NSLayoutAttribute.trailing, multiplier: 1, constant: sizeConst);
        trailingConstraint.priority = 999;
        self.view.addConstraint(trailingConstraint);
        
        let bottomConstraint = NSLayoutConstraint(item: self.coordinateView, attribute: NSLayoutAttribute.bottomMargin, relatedBy: NSLayoutRelation.equal, toItem: self.bottomView, attribute: NSLayoutAttribute.top, multiplier: 1, constant: sizeConst);
        self.view.addConstraint(bottomConstraint);
        
        let centerXConstraint = NSLayoutConstraint(item: self.view, attribute: NSLayoutAttribute.centerX, relatedBy: NSLayoutRelation.equal, toItem: self.coordinateView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0);
        self.view.addConstraint(centerXConstraint);
    }
    
    //MARK: - Current Image
    fileprivate func getCurrentImageToProcess() -> UIImage?
    {
        guard let _ = self.mainImageView?.image else
        {
            return nil;
        }
        
        return self.mainImageView?.image;
        
//        let scrollViewFrame =  self.scrollView.superview!.convertRect(self.scrollView.frame, toView:self.view);
//        UIGraphicsBeginImageContext(self.view.bounds.size);
//        let gContext = UIGraphicsGetCurrentContext();
//        self.view.layer.renderInContext(gContext!);
//        let image = UIGraphicsGetImageFromCurrentImageContext();
//        guard let croppedImageRef = CGImageCreateWithImageInRect(image.CGImage, scrollViewFrame) else
//        {
//            return nil;
//        }
//        let croppedImage = UIImage(CGImage: croppedImageRef);
//        
//        UIGraphicsEndImageContext();
//        return croppedImage;
    }
    
    fileprivate func centerImageInScrollView()
    {
        guard let imageView = mainImageView else
        {
            return
        }
        
        let boundsSize = scrollView.bounds.size;
        var contentsFrame = imageView.frame;
        
        if (contentsFrame.size.width < boundsSize.width)
        {
            contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0;
        }
        else
        {
            contentsFrame.origin.x = 0.0;
        }
        
        
        if (contentsFrame.size.height < boundsSize.height)
        {
            contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0;
        }
        else
        {
            contentsFrame.origin.y = 0.0;
        }
        
        imageView.frame = contentsFrame;
    }
    
    fileprivate func addImageHolder(_ image:UIImage?)
    {
        guard let fullImage = image else
        {
            print(" - Error: No image found.")
            return
        }
        
        for aSubview in scrollView.subviews
        {
            aSubview.removeFromSuperview();
        }
        
        self.scrollView.contentSize = scrollView.bounds.size;
        
        //create imageView
        self.mainImageView = UIImageView(image: fullImage);
        
        guard let imageView = mainImageView else
        {
            print(" - Error:  Did not create \"mainImageView\". ");
            return;
        }
        
        //position imageView to zoom and pan in scrollView
        imageView.sizeToFit();
        var fullFrame = imageView.frame;
        var maxScale: CGFloat = 3.0;
        
        if fullFrame.size.width > scrollView.bounds.size.width || fullFrame.size.height > scrollView.bounds.size.height
        {
            // decrease image frame to fit scrollView dimensions
            
            let horizontalRatio = scrollView.bounds.size.width / fullFrame.size.width;
            let verticalRatio = scrollView.bounds.size.height / fullFrame.size.height;
            
            let scaleFactor = (horizontalRatio > verticalRatio) ? horizontalRatio : verticalRatio;
            
            fullFrame.size.width *= scaleFactor;
            fullFrame.size.height *= scaleFactor;
            
            //setup scrollview insets at the top and bottom, or at the left and right
            var value = CGFloat(0.0);
            self.scrollView.contentSize = fullFrame.size;
            if fullFrame.height == scrollView.bounds.size.height // setup insets to left and right
            {
                let difference = fullFrame.size.width - self.scrollView.bounds.size.width;
                value = difference / 2.0;
                self.scrollView.contentOffset.x = value;
                //scrollView.contentInset = UIEdgeInsetsMake(0, value, 0.0, value)
            }
            else if fullFrame.width == scrollView.bounds.size.width //setup insets to top and bottom
            {
                let difference = fullFrame.size.height - self.scrollView.bounds.size.height;
                value = difference / 2.0;
                self.scrollView.contentOffset.y = value;
                //scrollView.contentInset = UIEdgeInsetsMake(value, 0.0, value, 0.0)
            }
            
            imageView.frame = fullFrame;
            
            maxScale = min(4.0,max(3.0, floor(1 / scaleFactor)));
        }
        else
        {
            imageView.frame = scrollView.bounds;
        }
        
        self.scrollView.maximumZoomScale = maxScale;
        self.scrollView.addSubview(imageView);
        
        let timeout:DispatchTime = DispatchTime.now() + Double(Int64(Double(NSEC_PER_SEC) * 1.0)) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: timeout, execute: {[weak self] () -> Void in
            self?.scrollView.flashScrollIndicators()
            })
    }
    
    override func setImageToProceed(_ image: UIImage) {
        self.originalImage = image;
        self.addImageHolder(self.originalImage);
    }
    
    //MARK: UITapRecogniger action
    func doubleTapAction(_ recognizer:UITapGestureRecognizer)
    {
        // Zoom out slightly, capping at the minimum zoom scale specified by the scroll view
        var newZoomScale:CGFloat = self.scrollView.zoomScale / 1.5;
        newZoomScale = max(newZoomScale, self.scrollView.minimumZoomScale);
        self.scrollView.setZoomScale(newZoomScale, animated: true);
        self.centerImageInScrollView();
    }
    
    //MARK: UIScrollViewDelegate
    func viewForZooming(in scrollView: UIScrollView) -> UIView?
    {
        return self.mainImageView;
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView)
    {
        self.coordinateView.onScrollChanged(scrollView.zoomScale,
                                            leftContentOffset: scrollView.contentOffset.x,
                                            topContentOffset: scrollView.contentOffset.y)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.coordinateView.onScrollChanged(scrollView.zoomScale,
                                            leftContentOffset: scrollView.contentOffset.x,
                                            topContentOffset: scrollView.contentOffset.y)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate
        {
            self.coordinateView.onScrollChanged(scrollView.zoomScale,
                                                leftContentOffset: scrollView.contentOffset.x,
                                                topContentOffset: scrollView.contentOffset.y)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.coordinateView.onScrollChanged(scrollView.zoomScale,
                                            leftContentOffset: scrollView.contentOffset.x,
                                            topContentOffset: scrollView.contentOffset.y)
    }
    
    //MARK: - JigPixAlgorithm
    func startProcess(_ targetImage:UIImage) {
        
        let sourceImage = self.getPuzzleImageById(puzzleImageIdx);
        let priority = DispatchQoS.QoSClass.userInitiated //the lower qos class - the slower image processing
        
        DispatchQueue.global(qos: priority).async {[unowned self] in
            self.algorithm.Start(sourceImage, target: targetImage);
        }
        
        self.coordinateView.ShowGrid(true, columns: JigpixAlgoritm.COLS, rows: JigpixAlgoritm.ROWS);
    }
    
    //MARK: - JigPixAlgorithmDelegate
    func jigpixAlgoritmDidStart(_ image: UIImage) {
        self.mainImageView?.image = image;
    }
    
    func jigpixAlgoritmImageChanged(_ image: UIImage) {
        self.mainImageView?.image = image;
    }
    
    func jigpixAlgoritmDidEnd(_ image: UIImage) {
        self.mainImageView?.image = image;
        self.bottomView?.convertButton.sendActions(for: UIControlEvents.touchUpInside);
    }
    
    //MARK: - Get Code
    fileprivate func rotateMainImageView()
    {
        let bgQueue = DispatchQueue(label: "CodeImageQueue", attributes: [])
        bgQueue.async{[weak self] in
            guard let image = self?.algorithm.GetCode() else
            {
                return;
            }
            DispatchQueue.main.async{[weak self] in
                self?.rotateMainImageViewWith(image);
            }
        }
    }
    
    fileprivate func rotateMainImageViewWith(_ image:UIImage)
    {
        self.scrollView.zoomScale = 1.0;
        
        let layer = self.mainImageView!.layer;
        var rotationAndPerspectiveTransformStart = CATransform3DIdentity;
        rotationAndPerspectiveTransformStart.m34 = 1.0 / 1000;
        rotationAndPerspectiveTransformStart = CATransform3DRotate(rotationAndPerspectiveTransformStart, CGFloat(Double.pi / 2.0), 0.0, -1.0, 0.0);
        UIView.animate(withDuration: 0.5, animations: {
            layer.transform = rotationAndPerspectiveTransformStart;
            }, completion: {[weak self]
                (value: Bool) in
                self?.mainImageView?.image = image;
                layer.transform = CATransform3DIdentity;
                var rotationAndPerspectiveTransformEnd = CATransform3DIdentity;
                rotationAndPerspectiveTransformEnd.m34 = 1.0 / 1000;
                rotationAndPerspectiveTransformEnd = CATransform3DRotate(rotationAndPerspectiveTransformEnd, CGFloat(Double.pi / 2.0), 0.0, 1.0, 0.0);
                layer.transform = rotationAndPerspectiveTransformEnd;
                UIView.animate(withDuration: 0.5, animations: {
                    layer.transform = CATransform3DIdentity;
                });
            });
    }
}

struct ToDeleteLayoutConstraints {
    var view: UIView!;
    var constraint: NSLayoutConstraint!
}

