//
//  CoordinateGridPosition.swift
//  JigsawPuzzleApp
//
//  Created by Ivan Yavorin on 12/20/15.
//  Copyright © 2015 JigSaw. All rights reserved.
//

import Foundation

struct CoordinateGridPosition:OptionSet {
    
    let rawValue:Int
    
    init(rawValue:Int)
    {
        self.rawValue = rawValue
    }
    
    static let Top =    CoordinateGridPosition(rawValue: 1)
    static let Left =   CoordinateGridPosition(rawValue: 2)
    static let Bottom = CoordinateGridPosition(rawValue: 3)
    static let Right =  CoordinateGridPosition(rawValue: 4)
    
    func isVertical() -> Bool
    {
        return (self.rawValue > 1 && self.rawValue % 2 == 0)
    }
}
