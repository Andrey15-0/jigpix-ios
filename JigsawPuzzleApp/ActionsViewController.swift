//
//  ActionsViewController.swift
//  Jigpix
//
//  Created by Admin on 3/19/16.
//  Copyright © 2016 UNIPUZZLE, INC. All rights reserved.
//

import Foundation
import MessageUI
import AVFoundation
import Photos
import Social

class ActionsViewController: UIViewController, MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    fileprivate let isPad = UIDevice.current.userInterfaceIdiom == .pad;
    internal lazy var needRefresh: Bool = true;
    
    fileprivate func createActionsDialog(_ image: UIImage)->UIAlertController
    {
        let actionsAlert = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet);
        
        //        let attrTitle = FlexibleTitleButton.convertStringToAttributedColoredText("Actions");
        //        actionsAlert.setValue(attrTitle, forKey: "attributedTitle");
        
        let photosAction = UIAlertAction(title: "Photos", style: .default, handler:
            { action in
                
                let status = PHPhotoLibrary.authorizationStatus()
                switch status {
                case .authorized:
                    actionsAlert.dismiss(animated: true, completion: nil);
                    UIImageWriteToSavedPhotosAlbum(image, self, #selector(ActionsViewController.image(_:didFinishSavingWithError:contextInfo:)), nil);
                    break;
                case .denied, .restricted :
                    self.alertToEncourageAccess("Photos");
                    break;
                case .notDetermined:
                    PHPhotoLibrary.requestAuthorization() { (status) -> Void in
                        if status == .authorized
                        {
                            // won't happen but still
                            actionsAlert.dismiss(animated: true, completion: nil);
                            UIImageWriteToSavedPhotosAlbum(image, self, #selector(ActionsViewController.image(_:didFinishSavingWithError:contextInfo:)), nil);
                        }
                    }
                }
        });
        
        let messageAction = UIAlertAction(title: "Message", style: .default, handler:
            { action in
                actionsAlert.dismiss(animated: true, completion: nil);
                
                if (MFMessageComposeViewController.canSendAttachments())
                {
                    let controller = MFMessageComposeViewController();
                    //controller.recipients = [phoneNumber.text]
                    controller.body = "Send from Jigpix";
                    let imageData: Data = UIImageJPEGRepresentation(image, 1.0)!;
                    controller.addAttachmentData(imageData, typeIdentifier: "image/jpeg", filename: "image.jpeg");
                    controller.messageComposeDelegate = self;
                    self.present(controller, animated: true, completion: nil);
                    self.needRefresh = false;
                }
                else
                {
                    self.showSendMessageErrorAlert();
                }
        });
        
        let mailAction = UIAlertAction(title: "Mail", style: .default, handler:
            { action in
                actionsAlert.dismiss(animated: true, completion: nil);
                if MFMailComposeViewController.canSendMail()
                {
                    let controller = MFMailComposeViewController();
                    //controller.setToRecipients(["nurdin@gmail.com"]);
                    controller.setSubject("Sending you e-mail from Jigpix");
                    let htmlMsg = "<html><body><p>Your message here...</p></body></html>";
                    controller.setMessageBody(htmlMsg, isHTML: true);
                    let imageData: Data = UIImageJPEGRepresentation(image, 1.0)!;
                    controller.addAttachmentData(imageData, mimeType: "image/jpeg", fileName: "image.jpeg");
                    controller.mailComposeDelegate = self;
                    self.present(controller, animated: true, completion: nil);
                    self.needRefresh = false;
                }
                else
                {
                    self.showSendMailErrorAlert();
                }
        });
        
        let facebookAction = UIAlertAction(title: "Facebook", style: .default, handler:
            { action in
                actionsAlert.dismiss(animated: true, completion: nil);
                
                let composeSheet = SLComposeViewController(forServiceType: SLServiceTypeFacebook);
                composeSheet?.setInitialText("Send from Jigpix");
                composeSheet?.add(image);
                
                self.present(composeSheet!, animated: true, completion: nil);
        });
        
        let instagramAction = UIAlertAction(title: "Instagram", style: .default, handler:
            { action in
                actionsAlert.dismiss(animated: true, completion: nil);
                
                InstagramManager.sharedManager.postImageToInstagramWithCaption(image, instagramCaption: "Send from Jigpix", view: self.view);
        });
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
            { action in
                actionsAlert.dismiss(animated: true, completion: nil);
        });
        
        let rowHeight = CGFloat(56);
        let x = CGFloat(10);
        let size = CGFloat(40);
        
        let photosImage = UIImageView(frame: CGRect(x: x, y: x, width: size, height: size));
        photosImage.image = UIImage(named: "Photos");
        let messageImage = UIImageView(frame: CGRect(x: x, y: rowHeight + x, width: size, height: size));
        messageImage.image  = UIImage(named: "Message");
        let mailImage = UIImageView(frame: CGRect(x: x, y: 2 * rowHeight + x, width: size, height: size));
        mailImage.image = UIImage(named: "Mail");
        let facebookImage = UIImageView(frame: CGRect(x: x, y: 3 * rowHeight + x, width: size, height: size))
        facebookImage.image = UIImage(named: "Facebook");
        let instagramImage = UIImageView(frame: CGRect(x: x, y: 4 * rowHeight + x, width: size, height: size))
        instagramImage.image = UIImage(named: "Instagram");
        
        actionsAlert.view.addSubview(photosImage);
        actionsAlert.view.addSubview(messageImage);
        actionsAlert.view.addSubview(mailImage);
        actionsAlert.view.addSubview(facebookImage);
        actionsAlert.view.addSubview(instagramImage);
        
        actionsAlert.addAction(photosAction);
        actionsAlert.addAction(messageAction);
        actionsAlert.addAction(mailAction);
        actionsAlert.addAction(facebookAction);
        actionsAlert.addAction(instagramAction);
        actionsAlert.addAction(cancelAction);
        
        return actionsAlert;
    }
    
    fileprivate func openPhoto()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)
        {
            let imagePicker = UIImagePickerController();
            imagePicker.delegate = self;
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
            imagePicker.allowsEditing = false;
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    fileprivate func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
        {
            let imagePicker = UIImagePickerController();
            imagePicker.delegate = self;
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
            imagePicker.allowsEditing = false;
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func alertToEncourageAccess(_ text: String)
    {
        //Camera not available - Alert
        let cameraUnavailableAlertController = UIAlertController (title: "\(text) Unavailable", message: "Please check to see if it is disconnected or in use by another application", preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .destructive) { (_) -> Void in
            let settingsUrl = URL(string:UIApplicationOpenSettingsURLString)
            if let url = settingsUrl {
                DispatchQueue.main.async {
                    UIApplication.shared.openURL(url)
                }
                
            }
        }
        let cancelAction = UIAlertAction(title: "Okay", style: .default, handler: nil)
        cameraUnavailableAlertController .addAction(settingsAction)
        cameraUnavailableAlertController .addAction(cancelAction)
        self.present(cameraUnavailableAlertController , animated: true, completion: nil)
    }
    
    fileprivate func createCameraDialog()->UIAlertController
    {
        let cameraAlert = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet);
        
        let libraryAction = UIAlertAction(title: "Library", style: .default, handler:
            { action in
                cameraAlert.dismiss(animated: true, completion: nil);
                
                let status = PHPhotoLibrary.authorizationStatus()
                switch status {
                case .authorized:
                    self.openPhoto();
                    break;
                case .denied, .restricted :
                    self.alertToEncourageAccess("Library");
                    break;
                case .notDetermined:
                    PHPhotoLibrary.requestAuthorization() { (status) -> Void in
                        if status == .authorized
                        {
                            self.openPhoto();
                        }
                    }
                }
        });
        
        let photoAction = UIAlertAction(title: "Photo", style: .default, handler:
            { action in
                cameraAlert.dismiss(animated: true, completion: nil);
                
                let cameraMediaType = AVMediaTypeVideo
                let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(forMediaType: cameraMediaType)
                
                switch cameraAuthorizationStatus {
                    
                case .authorized:
                    self.openCamera();
                    break
                case .restricted, .denied, .notDetermined:
                    // Prompting user for the permission to use the camera.
                    AVCaptureDevice.requestAccess(forMediaType: cameraMediaType) { granted in
                        if granted {
                            self.openCamera();
                        } else {
                            //Show Camera Unavailable Alert
                            self.alertToEncourageAccess("Photo");
                        }
                    }
                }
        });
        
        let exampleAction = UIAlertAction(title: "Example", style: .default, handler:
            { action in
                cameraAlert.dismiss(animated: true, completion: nil);
                
                self.setImageToProceed(UIImage(named: kStartImage)!);
        });
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
            { action in
                cameraAlert.dismiss(animated: true, completion: nil);
        });
        
        let rowHeight = CGFloat(56);
        let x = CGFloat(10);
        let size = CGFloat(40);
        
        let photosImage = UIImageView(frame: CGRect(x: x, y: x, width: size, height: size));
        photosImage.image = UIImage(named: "Photos");
        let cameraImage = UIImageView(frame: CGRect(x: x, y: rowHeight + x, width: size, height: size));
        cameraImage.image = UIImage(named: "Camera");
        let exampleImage = UIImageView(frame: CGRect(x: x, y: 2 * rowHeight + x, width: size, height: size));
        exampleImage.image = UIImage(named: "Example");
        
        cameraAlert.view.addSubview(photosImage);
        cameraAlert.view.addSubview(cameraImage);
        cameraAlert.view.addSubview(exampleImage);
        
        cameraAlert.addAction(libraryAction);
        cameraAlert.addAction(photoAction);
        cameraAlert.addAction(exampleAction);
        cameraAlert.addAction(cancelAction);
        
        return cameraAlert;
    }
    
    fileprivate func createInfoDialog()->UIAlertController
    {
        let message = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
        let infoAlert = UIAlertController(title: nil, message: message, preferredStyle: UIAlertControllerStyle.actionSheet);
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
            { action in
                infoAlert.dismiss(animated: true, completion: nil);
        });
        
        infoAlert.addAction(cancelAction);
        
        return infoAlert;
    }
    
    func showCameraDialog(_ sender: UIButton)
    {
        let cameraAlert = self.createCameraDialog();
        
        if(self.isPad)
        {
            cameraAlert.popoverPresentationController?.sourceView = sender;
            cameraAlert.popoverPresentationController?.sourceRect = sender.bounds;
            cameraAlert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.any;
        }
        
        self.present(cameraAlert, animated: true, completion: nil);
    }
    
    func showActionsDialog(_ sender: UIButton, image: UIImage)
    {
        let actionsAlert = self.createActionsDialog(image);
        
        if(self.isPad)
        {
            actionsAlert.popoverPresentationController?.sourceView = sender;
            actionsAlert.popoverPresentationController?.sourceRect = sender.bounds;
            actionsAlert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.any;
        }
        
        self.present(actionsAlert, animated: true, completion: nil);
    }
    
    func showInfoDialog(_ sender: UIButton)
    {
        let infoAlert = self.createInfoDialog();
        
        if(self.isPad)
        {
            infoAlert.popoverPresentationController?.sourceView = sender;
            infoAlert.popoverPresentationController?.sourceRect = sender.bounds;
            infoAlert.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.any;
        }
        
        self.present(infoAlert, animated: true, completion: nil);
    }
    
    func image(_ image: UIImage, didFinishSavingWithError error: NSError?, contextInfo:UnsafeRawPointer)
    {
        if error == nil {
            let ac = UIAlertController(title: "Saved!", message: "Your altered image has been saved to your photos.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(ac, animated: true, completion: nil)
        } else {
            let ac = UIAlertController(title: "Save error", message: error?.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(ac, animated: true, completion: nil)
        }
    }
    
    func showSendMessageErrorAlert()
    {
        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Message", message: "Your device could not send messages. Please check messsage configuration and try again.", delegate: self, cancelButtonTitle: "OK")
        sendMailErrorAlert.show();
    }
    
    func showSendMailErrorAlert()
    {
        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail. Please check e-mail configuration and try again.", delegate: self, cancelButtonTitle: "OK")
        sendMailErrorAlert.show();
    }
    
    // MARK: MFMessageComposeViewControllerDelegate
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult)
    {
        //... handle sms screen actions
        controller.dismiss(animated: true, completion: nil);
    }
    
    // MARK: MFMailComposeViewControllerDelegate
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?)
    {
        //... handle mail screen actions
        controller.dismiss(animated: true, completion: nil);
    }
    
    //MARK: UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!){
        self.dismiss(animated: true, completion: { () -> Void in
            
        })
        
        setImageToProceed(image);
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.needRefresh = false;
        self.dismiss(animated: true, completion: nil);
    }
    
    func setImageToProceed(_ image: UIImage)
    {
        
    }
    
    func showMemoryOutOfMemoryMessage()
    {
        // Create a UIAlertController.
        // ... Use Alert style.
        let dialog = UIAlertController(title: "Convertation stopped.",
                                       message: "Your Device out of memory!",
                                       preferredStyle: UIAlertControllerStyle.alert);
        
        dialog.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil));
        // Present the dialog.
        // ... Do not worry about animated or completion for now.
        present(dialog, animated: true, completion: nil);
    }
    
    //MARK: Get UIImage puzzleImage
    func getPuzzleImageById(_ idx: Int)->UIImage
    {
        var result: UIImage = UIImage(named: JigpixAlgoritm.puzzlePieces == Pieces.small300 ? "Venus Birth 300.png" : "Venus Birth 520.png")!;
        let size = result.size;
        
        if(idx == 0)
        {
            result = UIImage(named: "Embarrassing Politicians.png")!.resizeImage2(size);
        }
        else if(idx == 1)
        {
            result = UIImage(named: "Cute Animals.png")!.resizeImage2(size);
        }
        else if(idx > 2)
        {
            result = UIImage(named: items[idx])!.resizeImage2(size);
        }
        
        return result;
    }
}
