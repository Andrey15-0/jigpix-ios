//
//  SplachScreenViewController.swift
//  JigsawPuzzleApp
//
//  Created by CloudCraft on 1/5/16.
//  Copyright © 2016 JigSaw. All rights reserved.
//

import UIKit

let kShowSelectScreen = "ShowSelectScreen";
let kShowMainScreen = "ShowMainScreen";
let kShowMainScreenPad = "ShowMainScreenPad";
let kStartImage = "mona_lisa1000x1300";

class SplashScreenViewController: UIViewController {
    
    @IBOutlet weak var logoImageView:UIImageView!
    @IBOutlet weak var tapSomeWhereLabel:UILabel!
    @IBOutlet weak var blackWhiteModeButton:UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.blackWhiteModeButton.isHidden = true;
        
        self.tapSomeWhereLabel.alpha = 0;
        UIView.animate(withDuration: 1.0,
                                   delay:0.5,
                                   options: [UIViewAnimationOptions.repeat , UIViewAnimationOptions.autoreverse],
                                   animations: {self.tapSomeWhereLabel.alpha = 1},
                                   completion: nil);
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden : Bool {
        return true;
    }
    
    @IBAction func tapGestureAction(_ sender:UITapGestureRecognizer)
    {
        JigpixAlgoritm.isBlackWhite = false;
        self.performSegue(withIdentifier: kShowSelectScreen, sender: sender);
    }
    
    @IBAction func blackWhiteModeButtonAction(_ sender:UIButton)
    {
        JigpixAlgoritm.isBlackWhite = true;
        self.performSegue(withIdentifier: kShowSelectScreen, sender: sender);
    }
}
