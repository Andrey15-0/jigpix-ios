//
//  JigpixAlgoritm.swift
//  Jigpix
//
//  Created by Admin on 12/30/15.
//  Copyright © 2015 opes. All rights reserved.
//

import Foundation
import UIKit

//MARK: - Protocol JigpixAlgoritmDelegate
protocol JigpixAlgoritmDelegate {
    func jigpixAlgoritmDidStart(_ image: UIImage)
    func jigpixAlgoritmImageChanged(_ image: UIImage)
    func jigpixAlgoritmDidEnd(_ image: UIImage)
}

//MARK: - Enum Pieces
enum Pieces
{
    case small300
    case medium520
}

class JigpixAlgoritm
{
    internal static var puzzlePieces = Pieces.medium520;
    
    internal static var isBlackWhite = false;
    
    var delegate: JigpixAlgoritmDelegate? = nil;
    
    fileprivate var puzzle = Puzzle();
    
    fileprivate var outputImage: UIImage? = nil;
    fileprivate var codeImage: UIImage? = nil;
    
    //MARK: - Consts
    internal static var COLS : Int { return JigpixAlgoritm.puzzlePieces == Pieces.small300 ? 15 : 20; }
    internal static var ROWS : Int { return JigpixAlgoritm.puzzlePieces == Pieces.small300 ? 20 : 26; }
    
    var stopWorking = false;
    
    //MARK: - Start
    func Start(_ source: UIImage, target: UIImage)
    {
        self.stopWorking = false;
        
        self.outputImage = source;
        if(delegate != nil)
        {
            DispatchQueue.main.sync {
                self.delegate!.jigpixAlgoritmDidStart(self.outputImage!);
            }
        }
        
        self.puzzle.setup(JigpixAlgoritm.COLS, rows: JigpixAlgoritm.ROWS);
        
        self.puzzle.setFromImage(source, target: false);
        self.puzzle.setFromImage(target, target: true);
        
        self.puzzle.centralImortance = 60.0;
        self.puzzle.powerFunc = 3.0;
        
        var swapped = false;
        let refreshCnt = 10000
        var cnt = 0;
//        var i = 0;
        for _ in 0..<1300000
        {
            if(self.stopWorking)
            {
                break;
            }
            
            if(self.puzzle.tryToSwapStuff())
            {
                swapped = true;
            }
//            i=i+1;
            cnt = cnt + 1;
            if(cnt >= refreshCnt)
            {
                self.changeImage();
                cnt = 0;
                if(swapped == false)
                {
                    break;
                }
                swapped = false;
                //print(i);
            }
        }
        
//        print("Main cycle cnt=\(i)");
        
        //SAVE OBJECT TO OUTPUT IMAGE
        self.saveOutputImage(true);
        
        //GENERATE CODE IMAGES PICTURE
        if(JigpixAlgoritm.puzzlePieces == Pieces.small300)
        {
            let cropImage = UIImage(named: "Jigpix_300.png")!;
            self.generateCodeImage(cropImage);
        }
        else
        {
            let cropImage = UIImage(named: "Jigpix_520_2.png")!;
            self.generateCodeImage(cropImage);
        }
        
        if(delegate != nil)
        {
            DispatchQueue.main.sync {
                self.delegate!.jigpixAlgoritmDidEnd(self.outputImage!);
            }
        }
    }
    
    //MARK: - Fire image changed event with saving
    func changeImage()
    {
        self.saveOutputImage();
        
        if(delegate != nil)
        {
            DispatchQueue.main.async {
                self.delegate!.jigpixAlgoritmImageChanged(self.outputImage!);
            }
        }
    }
    
    //MARK: - Saves canvas to output image
    func saveOutputImage(_ last: Bool = false)
    {
        let COLS = self.puzzle.nPiecesHoriz;
        let ROWS = self.puzzle.nPiecesVert;
        let pieceSizeW = self.puzzle.smallResHoriz / self.puzzle.nPiecesHoriz;
        let centers = pieceSizeW * (last ? 2 : 1);//Int(self.puzzle.pieces[0].img!.size.width) * (last ? 2 : 1);
        let sizeResult = CGSize(width: centers * COLS, height: centers * ROWS);
        
        let opaque = false;
        let scale: CGFloat = 0;
        UIGraphicsBeginImageContextWithOptions(sizeResult, opaque, scale);
        let context = UIGraphicsGetCurrentContext();
        
        let rectangle = CGRect(x: 0, y: 0, width: sizeResult.width, height: sizeResult.height);
        context?.setFillColor(UIColor.white.cgColor);
        context?.addRect(rectangle);
        context?.drawPath(using: CGPathDrawingMode.fill);
        
        context?.setStrokeColor(UIColor.black.cgColor);
        context?.setLineWidth(1);
        for p in 0..<self.puzzle.nTotalPieces
        {
            let row = p / COLS;
            let col = p % COLS;
            
            var pieceImage = self.puzzle.pieces[p].img!;
            let degrees = self.puzzle.pieces[p].rotation * 90;
            if(degrees > 0)
            {
                pieceImage = pieceImage.rotate(degrees);
            }
            let drawRect = CGRect(x: col * centers, y: row * centers, width: centers, height: centers);
            pieceImage = last ? pieceImage.resizeImage2(CGSize(width: centers, height: centers)) : pieceImage;
            pieceImage.draw(in: drawRect);
            context?.addRect(drawRect);
            context?.drawPath(using: CGPathDrawingMode.stroke);
            
//            if last
//            {
//                print("NO=\(p + 1) ID=\(self.puzzle.pieces[p].id) R=\(self.puzzle.pieces[p].rotation)");
//            }
        }
        
        self.outputImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    //MARK: - Generate codes image
    fileprivate func generateCodeImage(_ cropImage: UIImage)
    {
        self.codeImage = nil;
        
        let centers = Int(cropImage.size.width) / JigpixAlgoritm.COLS;
        let size = CGSize(width: centers * JigpixAlgoritm.COLS, height: centers * JigpixAlgoritm.ROWS);
        
        // Setup our context
        let opaque = false;
        let scale: CGFloat = 0;
        UIGraphicsBeginImageContextWithOptions(size, opaque, scale);
        let context = UIGraphicsGetCurrentContext();
        
        let rectangle = CGRect(x: 0, y: 0, width: size.width, height: size.height);
        context?.setFillColor(UIColor.white.cgColor);
        context?.addRect(rectangle);
        context?.drawPath(using: CGPathDrawingMode.fill);
        
        //CGContextSetFillColorWithColor(context, UIColor.whiteColor().CGColor);
        context?.setStrokeColor(UIColor.black.cgColor);
        context?.setLineWidth(3);
        for p in 0..<self.puzzle.nTotalPieces
        {
            let row = p / self.puzzle.nPiecesHoriz;
            let col = p % self.puzzle.nPiecesHoriz;
            let piece = self.puzzle.pieces[p];
            let pieceNo = piece.id;
            
            let cropRect = CGRect(x: pieceNo % self.puzzle.nPiecesHoriz * centers,
                                  y: pieceNo / self.puzzle.nPiecesHoriz * centers,
                                  width: centers,
                                  height: centers);
            var image = cropImage.cropImage(cropRect);
            let angle = 90 * piece.rotation;
            if(angle != 0)
            {
                image = image.rotate(angle);//.imageRotatedByDegrees(CGFloat(angle), flip: false);
            }
            
            let drawRect = CGRect(x: col * centers, y: row * centers, width: centers, height: centers);
            image.draw(in: drawRect);
            context?.addRect(drawRect);
            context?.drawPath(using: CGPathDrawingMode.stroke);
        }
        
        // Drawing complete, retrieve the finished image and cleanup
        self.codeImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    //MARK: - Get codes image
    func GetCode() -> UIImage
    {
        return self.codeImage!;
    }
}
