//
//  puzzlePiece.swift
//  Jigpix
//
//  Created by Admin on 4/30/16.
//  Copyright © 2016 UNIPUZZLE, INC. All rights reserved.
//

import Foundation
import UIKit

let CHOPSIZE = 15;
let TILEPIXELS = CHOPSIZE * CHOPSIZE;

class PuzzlePiece : CustomStringConvertible {
    
    //MARK: - Struct PixelData
    struct PixelData {
        var a = 0;
        var r = 0;
        var g = 0;
        var b = 0;
        var avg = 0;
    }
    
    var img: UIImage? = nil;
    var colorImgPixData = [[[PixelData]]]();
    
    var rotation: Int = 0;					//0 1 2 3 clockwise;
    
    var bri: Int = 0;  /* average: (red+gre+blu)/3                         */
    var gre: Int = 0;  /* average green                                    */
    var rmb: Int = 0;  /* average red - average blue                       */
    var rit: Int = 0;  /* increase in (r+g+b)/3 center to mid right  edge  */
    var bot: Int = 0;  /* increase in (r+g+b)/3 center to mid bottom edge  */
    
    var id: Int = 0;	// my ORIGINAL id.
    
    var distanceFromCenter: Double = 0.0;
    
    var description: String {
        get {
            return "SN='\(id)' BRI='\(bri)' GRE='\(gre)' RMB='\(rmb)' RIT='\(rit)' BOT='\(bot)'";
        }
    }
    
    func processTile(_ pixels: [[PixelData]])
    {
        var red = 0, gre = 0, blu = 0, bri = 0;         /* vals per pixel 0-255 */
        var redsum = 0, gresum = 0, blusum = 0, brisum = 0;      /* sums of 15x15 pixels */
        var rit75 = 0, lef75 = 0, top75 = 0, bot75 = 0;       /* sums of 75 bri's     */
        var redavg = 0, greavg = 0, bluavg = 0, briavg = 0;
        
        /* array is 1-D, let's pretend its 2-D for the moment */
        for y in 0..<15
        {
            for x in 0..<15
            {
                /* note----> ++ sequences thru R,G,B, R,G,B values */
                red = pixels[y][x].r;        redsum += red;
                gre = pixels[y][x].g;        gresum += gre;
                blu = pixels[y][x].b;        blusum += blu;
                bri = red+gre+blu;           brisum += bri; /* 3x */
                
                /* sums of left & right & top & bot thirds */
                if(x < 5){lef75 += bri;}
                if(x > 9){rit75 += bri;}
                if(y < 5){top75 += bri;}
                if(y > 9){bot75 += bri;}
            }
        }
        
        redavg = redsum/TILEPIXELS;
        greavg = gresum/TILEPIXELS;
        bluavg = blusum/TILEPIXELS;
        briavg = brisum/(TILEPIXELS * 3);       /* was 3x */
        
        /* load results into TILE struct */
        bri = briavg;
        gre = greavg;
        rmb = redavg - bluavg;
        rit = ((rit75 - lef75)*7)/10;       /* WARNING: specific */
        bot = ((bot75 - top75)*7)/10;       /* for 15 x 15 tiles */
        
        //print(" tile info %i %i %i %i %i \n",  tptr.bri, tptr.gre,  tptr.rmb, tptr.rit, tptr.bot);
        
        /* note on (( )*7)/10 The centers of left and right sample areas are
         * 10 pixels apart; the right edge pixel is only 7 pixels right of
         * center, thus the factor 7/10. Likewise for top and bottom samples
         */
        
        //print("%i %i %i %i %i \n", tptr.bri, tptr.gre, tptr.rmb, tptr.rit, tptr.bot);
        
        return;
    }
    
    func getPixels(_ image: UIImage) -> [[PixelData]]
    {
        let inImage:CGImage = image.cgImage!;
        let context = image.createARGBBitmapContext(inImage);
        let pixelsWide = inImage.width;
        let pixelsHigh = inImage.height;
        let rect = CGRect(x:0, y:0, width:Int(pixelsWide), height:Int(pixelsHigh));
        
        var pic = Array(repeating: Array(repeating: PixelData(), count: pixelsWide), count: pixelsHigh);
        
        //Clear the context
        context.clear(rect);
        
        // Draw the image to the bitmap context. Once we draw, the memory
        // allocated for the context for rendering will then contain the
        // raw image data in the specified color space.
        context.draw(inImage, in: rect);
        
        // Now we can get a pointer to the image data associated with the bitmap
        // context.
        
        let data = context.data!
        let dataType = data.bindMemory(to: UInt8.self, capacity: pixelsWide * pixelsHigh)
        
        for x in 0..<Int(pixelsWide)
        {
            for y in 0..<Int(pixelsHigh)
            {
                let offset = 4*((Int(pixelsWide) * Int(y)) + Int(x));
                let alpha = dataType[offset];
                let red = dataType[offset+1];
                let green = dataType[offset+2];
                let blue = dataType[offset+3];
                
                let avg = (UInt32(red) + UInt32(green) + UInt32(blue))/3;
                
                pic[y][x].avg = Int(avg);
                pic[y][x].a = Int(alpha);
                pic[y][x].r = Int(red);
                pic[y][x].g = Int(green);
                pic[y][x].b = Int(blue);
            }
        }
        
        return pic;
    }
    
    func calculateStatistics(_ imagePiece: UIImage, cols: Int, rows: Int)
    {
        self.rotation = 0;
        
        self.img = imagePiece;
        
        for i in 0..<4
        {
            let tmpImg = self.img?.resizeImage2(CGSize(width: 5,height: 5)).rotate((360 - i*90));
            let pix = self.getPixels(tmpImg!);
            self.colorImgPixData.append(pix);
        }
        
        let pixelData = self.getPixels(self.img!);
        
        self.processTile(pixelData);
        
        let nPiecesHoriz = cols;
        let nPiecesVert = rows;
        
        let x = self.id % nPiecesHoriz;
        let y = self.id / nPiecesHoriz;
        
        var distancex: Double = 0.0;
        if (x < (nPiecesHoriz/2)){
            distancex = Double(x) / Double(nPiecesHoriz/2);  // 0 - 1 to center
        } else {
            distancex = 1 - Double(x - nPiecesHoriz/2) / Double(nPiecesHoriz/2);  // 1 - 0 from center
        }
        
        var distancey: Double = 0.0;
        if (y < (nPiecesVert/2)){
            distancey = Double(y) / Double(nPiecesVert/2);  // 0 - 1 to center
        } else {
            distancey = 1 - Double(y - nPiecesVert/2) / Double(nPiecesVert/2);  // 1 - 0 from center
        }
        
        let maxDistance = min(distancex, distancey);
        
        self.distanceFromCenter = maxDistance;
        
        //print("id = %i, x y (%i %i), distance = %f \n", id, x, y, distanceFromCenter);
    }
    
}
