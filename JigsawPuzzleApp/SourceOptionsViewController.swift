//
//  SourceOptionsViewController.swift
//  JigsawPuzzleApp
//
//  Created by CloudCraft on 1/5/16.
//  Copyright © 2016 JigSaw. All rights reserved.
//

import UIKit

class SourceOptionsViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    let mainScreenSegue = "ShowMainScreen"
    var fullSizeImage:UIImage?
    private var imagePickerController:UIImagePickerController?
    
    @IBOutlet weak var folderButton:UIButton!
    @IBOutlet weak var cameraButton:UIButton!
    @IBOutlet weak var cancelButton:UIButton!
    
    lazy var pushAnimator = PushAnimator(transitionAnimationOperation: .Push)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if !UIImagePickerController.isSourceTypeAvailable(.Camera) {
            cameraButton.enabled = false;
        }
        // Do any additional setup after loading the view.
        self.cameraButton.layer.cornerRadius = 5.0;
        self.cameraButton.layer.masksToBounds = true;
        self.navigationItem.hidesBackButton = true;
        
        self.cancelButton.layer.cornerRadius = 5.0;
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated);
        
        self.showOrHideCancelButton();
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.showMainScreenIfImageIsPresent();
    }
    
    @IBAction func monaLisaTapGectureAction(tapRecognizer:UITapGestureRecognizer)
    {
        self.fullSizeImage = UIImage(named: "mona_lisa1000x1300") //UIImage(named: "Mona_Lisa_LARGE")
        showMainScreenIfImageIsPresent()
    }
    
    @IBAction func folderButtonAction(sender:UIButton)
    {
        presentImagePickerWithSource(UIImagePickerControllerSourceType.PhotoLibrary)
    }
    
    @IBAction func cameraButtonAction(sender:UIButton)
    {
        presentImagePickerWithSource(UIImagePickerControllerSourceType.Camera)
    }
    
    @IBAction func cancelButtonAction(sender:UIButton)
    {
        if(MainSceneViewController.prevStateView != nil)
        {
            self.navigationController?.pushViewController(MainSceneViewController.prevStateView!, animated: true);
        }
    }
    
    private func presentImagePickerWithSource(sourceType:UIImagePickerControllerSourceType)
    {
        self.imagePickerController = UIImagePickerController()
        
        guard let pickerVC = self.imagePickerController else
        {
            return
        }
        
        pickerVC.delegate = self
        
        if UIImagePickerController.isSourceTypeAvailable(sourceType)
        {
            pickerVC.sourceType = sourceType
            if sourceType == .Camera
            {
                pickerVC.showsCameraControls = true
            }
        }
        else
        {
            pickerVC.sourceType = .PhotoLibrary
        }
        
        self.presentViewController(pickerVC, animated: true, completion: nil)
    }
    
    private func showOrHideCancelButton()
    {
        self.cancelButton.hidden = MainSceneViewController.prevStateView == nil;
    }
    
    private func showMainScreenIfImageIsPresent()
    {
        if let imageToProcess = self.fullSizeImage
        {
            MainSceneViewController.prevStateView = nil;
            self.performSegueWithIdentifier(mainScreenSegue, sender: imageToProcess)
        }
    }
    
    //MARK: - UIImagePickerControllerDelegate
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        var selectedImage:UIImage = UIImage()
        if picker.allowsEditing
        {
            if let editedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
                selectedImage = editedImage
            }
            else if let originamImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                selectedImage = originamImage
            }
        }
        else
        {
            if let originamImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                selectedImage = originamImage
            }
        }
        
        proceedWithSelectedImage(selectedImage)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        
        picker.dismissViewControllerAnimated(true, completion: nil)
        self.imagePickerController = nil
    }
    
    //MARK: -
    func proceedWithSelectedImage(image:UIImage)
    {
        let bgQueue:dispatch_queue_t = dispatch_queue_create("JigSaw.ImageProcessing.Background", DISPATCH_QUEUE_SERIAL)
        
        dispatch_async(bgQueue) {[weak self] () -> Void in
            
            let fixedImage = image.fixOrientation()
            guard let _ = self else
            {
                return
            }
            
            dispatch_async(dispatch_get_main_queue()) {[weak self] () -> Void in
                if let aSelf = self
                {
                    aSelf.fullSizeImage = fixedImage
                    aSelf.imagePickerController?.dismissViewControllerAnimated(true, completion: nil)
                    aSelf.imagePickerController = nil
                }
            }//end of mainQueue
        }// end of BG queue
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == mainScreenSegue
        {
            if let senderImage = sender as? UIImage, destinationMainSceeneVC = segue.destinationViewController as? MainSceneViewController, let copyImage = senderImage.copy() as? UIImage
            {
                destinationMainSceeneVC.fullSizeImage = copyImage
                self.fullSizeImage = nil
            }
        }
    }
    
    
    //MARK: - UINavigationControllerDelegate
    func navigationController(navigationController: UINavigationController, animationControllerForOperation operation: UINavigationControllerOperation, fromViewController fromVC: UIViewController, toViewController toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        if let _ = navigationController as? UIImagePickerController
        {
            return nil
        }
        if operation == .None
        {
            return nil
        }
        
        if pushAnimator.transitionAnimationOperation != operation
        {
            pushAnimator.transitionAnimationOperation = operation
        }
        return self.pushAnimator
    }
    
    
    
}
