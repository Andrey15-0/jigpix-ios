//
//  MainNavigationController.swift
//  JigsawPuzzleApp
//
//  Created by Ivan Yavorin on 12/8/15.
//  Copyright © 2015 JigSaw. All rights reserved.
//

import UIKit

class MainNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationBar.barStyle = .Default
        self.navigationBar.translucent = true
        
        self.navigationBar.barTintColor = UIColor.clearColor()
        self.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationBar.backgroundColor = UIColor.clearColor()
        
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
