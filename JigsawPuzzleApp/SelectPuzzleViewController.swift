//
//  SelectPuzzleViewController.swift
//  Jigpix
//
//  Created by Admin on 5/18/16.
//  Copyright © 2016 UNIPUZZLE, INC. All rights reserved.
//

import Foundation
import UIKit

var items:[String] = ["first.png", "second.png", "third.png", "01-Trump.jpg", "02-Corbyn.jpg", "03-Markel.jpg", "Wallace-Gromit.jpg"];

class SelectPuzzleViewController: UIViewController, iCarouselDataSource, iCarouselDelegate
{
    @IBOutlet var carousel : iCarousel!;
    
    override func awakeFromNib()
    {
        super.awakeFromNib();
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad();
        self.carousel.type = .coverFlow;
        self.carousel.bounces = false;
        self.carousel.decelerationRate = 0.0;
    }
    
    override var prefersStatusBarHidden : Bool
    {
        return true;
    }
    
    func numberOfItems(in carousel: iCarousel) -> Int
    {
        return items.count;
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView
    {
//        var label: UILabel
        var itemView: UIImageView
//        var itemPicView: UIImageView
        
        //create new view if no view is available for recycling
        if (view == nil)
        {
            let sizeItemView = CGRect(x:0, y:0, width:self.view.bounds.width, height:self.view.bounds.height);
            itemView = UIImageView(frame:sizeItemView);
//            itemView.image = UIImage(named: "page.png");
//            itemView.layer.borderColor = UIColor.redColor().CGColor;
//            itemView.layer.borderWidth = 2;
            itemView.contentMode = .scaleAspectFit //.Center
            
//            label = UILabel(frame:itemView.bounds);
//            label.backgroundColor = UIColor.clearColor();
//            label.textColor = UIColor.redColor();
//            label.textAlignment = .Center;
//            label.font = label.font.fontWithSize(50);
//            label.tag = 1;
//            itemView.addSubview(label);
        }
        else
        {
            //get a reference to the label in the recycled view
            itemView = view as! UIImageView;
//            label = itemView.viewWithTag(1) as! UILabel!
        }
        
        //set item label
        //remember to always set any properties of your carousel item
        //views outside of the `if (view == nil) {...}` check otherwise
        //you'll get weird issues with carousel item content appearing
        //in the wrong place in the carousel
//        label.text = "\(index + 1)"
        itemView.image = UIImage(named: items[index]);
        
        return itemView
    }
    
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat
    {
        if (option == .spacing)
        {
            return value * 1.1
        }
        return value
    }
    
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int)
    {
        if(UIDevice.current.userInterfaceIdiom == .pad)
        {
            self.performSegue(withIdentifier: kShowMainScreenPad, sender: carousel);
        }
        else
        {
            self.performSegue(withIdentifier: kShowMainScreen, sender: carousel);
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let identifier = segue.identifier
        {
            switch identifier
            {
            case kShowMainScreenPad:
                if let destinationMainSceeneVC = segue.destination as? MainViewControllerPad
                {
                    JigpixAlgoritm.puzzlePieces = self.carousel.currentItemIndex == 0 ? Pieces.small300 : Pieces.medium520;
                    destinationMainSceeneVC.puzzleImageIdx = self.carousel.currentItemIndex;
                    destinationMainSceeneVC.originalImage = UIImage(named: kStartImage);
                }
                break
            case kShowMainScreen:
                if let destinationMainSceeneVC = segue.destination as? MainSceneViewController
                {
                    JigpixAlgoritm.puzzlePieces = self.carousel.currentItemIndex == 0 ? Pieces.small300 : Pieces.medium520;
                    destinationMainSceeneVC.puzzleImageIdx = self.carousel.currentItemIndex;
                    destinationMainSceeneVC.fullSizeImage = UIImage(named: kStartImage);
                }
                break;
            default:
                break
            }
        }
    }
}
