//
//  FlexibleTitleButton.swift
//  JigsawPuzzleApp
//
//  Created by CloudCraft on 1/6/16.
//  Copyright © 2016 JigSaw. All rights reserved.
//

import UIKit

let kDefaultFontName = "Chalkduster";

class FlexibleTitleButton: UIButton {

    var titleState:ConvertButtonState = .convert(title: "Convert"){
        didSet{
            var newTitle:String?
            //self.layer.borderWidth = 2.0
            switch titleState
            {
            case .convert(let title):
                newTitle = title
            case .converting(let title):
                newTitle = title
            case .done( let title):
                newTitle = title
            case .getCode(let title):
                newTitle = title
            }
            
            if(newTitle != nil)
            {
                let title = newTitle;
//                self.setTitle(title, forState: UIControlState.Normal);
                let attrText = FlexibleTitleButton.convertStringToAttributedColoredText(title!);
                self.setAttributedTitle(attrText, for: UIControlState())
                self.titleLabel!.textAlignment = NSTextAlignment.center;
                
                self.titleLabel!.numberOfLines = 0;
                self.titleLabel!.adjustsFontSizeToFitWidth = true;
            }
        }
    }
    
    func setNextTitle()
    {
        switch titleState{
        case .convert:
            titleState = .converting(title: "Converting...")
        case .converting:
            titleState = .getCode(title: "Get Code")
        case .getCode:
            titleState = .done(title: "Done")
        case .done:
            titleState = .convert(title: "Convert")
        }
    }
    
    func setPreviousTitle()
    {
        switch titleState
        {
            case .done(_):
                titleState = .getCode(title: "Get Code")
            case .getCode(_):
                titleState = .convert(title: "Convert")
            default:
                break
        }
    }

    func switchToDefaultState()
    {
        self.titleState = .convert(title:"Convert")
    }
    
//    override func drawRect(rect: CGRect) {
//        //1 - get the current context
//        let context = UIGraphicsGetCurrentContext();
//        var colors: [CGColor] = [];
//        colors.append(UIColor(red: 226.0/255.0, green: 1.0/255.0, blue: 127.0/255.0, alpha: 1.0).CGColor);
//        colors.append(UIColor(red: 234.0/255.0, green: 89.0/255.0, blue: 16.0/255.0, alpha: 1.0).CGColor);
//        colors.append(UIColor(red: 190.0/255.0, green: 217.0/255.0, blue: 65.0/255.0, alpha: 1.0).CGColor);
//        colors.append(UIColor(red: 21.0/255.0, green: 164.0/255.0, blue: 229.0/255.0, alpha: 1.0).CGColor);
//        colors.append(UIColor(red: 14.0/255.0, green: 99.0/255.0, blue: 51.0/255.0, alpha: 1.0).CGColor);
//        colors.append(UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0).CGColor);
//        
//        //2 - set up the color space
//        let colorSpace = CGColorSpaceCreateDeviceRGB();
//        
//        //3 - set up the color stops
//        let colorLocations:[CGFloat] = [0.0, 0.2, 0.4, 0.6, 0.8, 1.0];
//        
//        //4 - create the gradient
//        let gradient = CGGradientCreateWithColors(colorSpace,
//                                                  colors,
//                                                  colorLocations);
//        
//        let clipPath = UIBezierPath(roundedRect: rect, cornerRadius: 8.0);
//        clipPath.addClip();
//        
//        //5 - draw the gradient
//        let startPoint = CGPoint.zero;
//        let endPoint = CGPoint(x:self.bounds.width, y:0);
//        CGContextDrawLinearGradient(context,
//                                    gradient,
//                                    startPoint,
//                                    endPoint,
//                                    CGGradientDrawingOptions.DrawsAfterEndLocation);
//    }
    
    override func layoutSubviews() {

        self.layer.cornerRadius = 8.0;
//        self.layer.borderWidth = 0.0;
        self.layer.borderColor = UIColor.white.cgColor
        
        switch self.titleState
        {
            case .done(_):
                self.layer.borderWidth = 0.0;
            default:
                self.layer.borderWidth = 2.0;
        }
        
        super.layoutSubviews()
    }
    
    internal static func convertStringToAttributedColoredText(_ str: String) -> NSMutableAttributedString
    {
        var colors: [UIColor] = [];
        colors.append(UIColor(red: 226.0/255.0, green: 1.0/255.0, blue: 127.0/255.0, alpha: 1.0));
        colors.append(UIColor(red: 234.0/255.0, green: 89.0/255.0, blue: 16.0/255.0, alpha: 1.0));
        colors.append(UIColor(red: 190.0/255.0, green: 217.0/255.0, blue: 65.0/255.0, alpha: 1.0));
        colors.append(UIColor(red: 21.0/255.0, green: 164.0/255.0, blue: 229.0/255.0, alpha: 1.0));
        colors.append(UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0));
        colors.append(UIColor(red: 14.0/255.0, green: 99.0/255.0, blue: 51.0/255.0, alpha: 1.0));
        
        return convertStringToAttributedColoredText(str, colors: colors);
    }
    
    fileprivate static func isChar(_ char: Character, inSet set: CharacterSet) -> Bool {
        var found = true
        for ch in String(char).utf16 {
            if !set.contains(UnicodeScalar(ch)!) { found = false }
        }
        return found;
    }
    
    fileprivate static func convertStringToAttributedColoredText(_ str: String, colors: [UIColor]) -> NSMutableAttributedString
    {
        let whitespaceAndNewline = CharacterSet.whitespacesAndNewlines;
        let strWithoutWhitespaces = str.trimmingCharacters(in: whitespaceAndNewline);
        let length = max(strWithoutWhitespaces.characters.count / colors.count, 1);
        //let rest = str.characters.count - length;
        
        let fontSize = getFontSizeAccordingToDevice();
        let fontAttribute = [ NSFontAttributeName: UIFont(name: "Chalkduster", size: fontSize)! ];
        let attrText = NSMutableAttributedString(string: str, attributes: fontAttribute );
        
        for i in 0..<str.characters.count
        {
            let index = str.characters.index(str.startIndex, offsetBy: i);
            let character = str[index];
            if(isChar(character, inSet: whitespaceAndNewline))
            {
                continue;
            }
            else
            {
                let colorIdx = min(i / length, colors.count - 1);
                let color = colors[colorIdx];
                let range = NSRange(location: i, length: 1);
                attrText.addAttribute(NSForegroundColorAttributeName, value: color, range: range);
            }
        }
        
        return attrText;
    }
    
    fileprivate static func getFontSizeAccordingToDevice() -> CGFloat
    {
        var size = CGFloat(22);
        
        if DeviceType.IS_IPHONE_4_OR_LESS
        {
            size += 0;
        }
        if DeviceType.IS_IPHONE_5
        {
            size += 10;
        }
        else if DeviceType.IS_IPHONE_6
        {
            size += 28;
        }
        else if DeviceType.IS_IPHONE_6P
        {
            size += 38;
        }
        else if DeviceType.IS_IPAD
        {
            size += 40;
        }
        
        return size;
    }
}

struct ScreenSize
{
    static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType
{
    static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
}
