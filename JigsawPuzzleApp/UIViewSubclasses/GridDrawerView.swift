//
//  GridDrawerView.swift
//  Jigpix
//
//  Created by Ivan Yavorin on 1/24/16.
//  Copyright © 2016 UNIPUZZLE, INC. All rights reserved.
//

import UIKit

class GridDrawerView: UIView {

    lazy var gridDrawn = false
    
    fileprivate var gridCells:CGSize = CGSize.zero
    
    /**
     **gridCells** information is used as horizontal and vertical squares quantity, be sure to pass integer values (e.g 40.0, or 40) into this parameter.
     - values are evaluated as *ceil* from *gridCells* information `width` and `height`
    */
    convenience init(superView:UIView, gridCells:CGSize) {
        
        self.init(frame:superView.bounds)
        
        self.gridCells = CGSize(width: ceil(gridCells.width), height: ceil(gridCells.height))
        self.backgroundColor = UIColor.clear
    }
    
    override func draw(_ rect: CGRect) {
        UIColor.black.setStroke()
        drawGrid()
    }
    
    override func layoutSubviews() {
        
        super.layoutSubviews()
       
    }
    
    func drawGrid() {
        
        if gridDrawn
        {
            return
        }
        
        let gridLineThickness = CGFloat(1.5)
        
        let horizontalLinesCount = gridCells.width
        let verticalLinesCount = Int(gridCells.height) + 1
        
        let boundsHeight = self.bounds.size.height
        let boundsWidth = self.bounds.size.width
        
        let squareSide = boundsWidth / horizontalLinesCount
        
        var currentHorizontalOffset = CGFloat(0.0)
        
        for _ in 0..<verticalLinesCount
        {
            //top to bottom
            let startTopPoint = CGPoint(x: currentHorizontalOffset, y: 0.0)
            let finishBottomPoint = CGPoint(x: currentHorizontalOffset, y: boundsHeight)
            
            let verticalLinePath = UIBezierPath()
            verticalLinePath.lineWidth = gridLineThickness
            
            verticalLinePath.move(to: startTopPoint)
            verticalLinePath.addLine(to: finishBottomPoint)
            verticalLinePath.stroke()
            verticalLinePath.close()
            
            //left to right
            let startLeftPoint = CGPoint(x: 0.0, y: currentHorizontalOffset)
            let finishRightPoint = CGPoint(x: boundsWidth, y: currentHorizontalOffset)
            
            let horizontalLinePath = UIBezierPath()
            horizontalLinePath.lineWidth = gridLineThickness
            
            horizontalLinePath.move(to: startLeftPoint)
            horizontalLinePath.addLine(to: finishRightPoint)
            horizontalLinePath.stroke()
            horizontalLinePath.close()
            
            currentHorizontalOffset += squareSide
        }
        
        gridDrawn = true
    }

}
